<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    header('Content-Type: application/json');
    class Api extends CI_Controller {
        
        function __construct() {
            parent::__construct();
            $this->load->model('Apimodel');
        }
        
        // Admin Login for Admin Access
        // Created By: Siba Prasad Hota 
        // 11 June 2018 2:29 AM
        
        public function adminlogin(){
            $data = array(
                          'Admin_Email' => strtolower($this->input->get('email')),
                          'Admin_Password' => md5(trim($this->input->get('password')))
                          );
            $result = $this->Apimodel->checkAdminlogin($data);
            if ($result) {
                
                echo json_encode(array("status"=>'Success',"Msg"=>"Login Success","data"=>$result));
            }else {
                echo json_encode(array("status"=>'Failed',"Msg"=>'Invalid Username or Password'));
            }
        }
        
        
        // Customer Login for App Access
        // Created By: Siba Prasad Hota 
        // 11 June 2018 2:29 AM
        
         public function login(){
            $data = array(
                          'Cust_Email' => strtolower($this->input->get('email')),
                          'Cust_Password' => md5(trim($this->input->get('password')))
                          );
            $result = $this->Apimodel->checkCustomerlogin($data);
            if ($result) {
                echo json_encode(array("status"=>'Success',"Msg"=>"Login Success","data"=>$result));
            }else {
                echo json_encode(array("status"=>'Failed',"Msg"=>'Invalid Username or Password'));
            }
        }
        

        // Get Machine List for a Particular Customer
        // Created By: Siba Prasad Hota 
        // 11 June 2018 2:29 AM
        
        public function getmachines(){
            $result = $this->Apimodel->getCustomerMachineList($this->input->get('customerID'));
            if ($result) {
                echo json_encode(array("status"=>'Success',"Msg"=>"Machines Listed","data"=>$result));
            }else {
                echo json_encode(array("status"=>'Failed',"Msg"=>'No Machines added',"data"=>array()));
            }
        
        }
        
        
        
        // getCustomerMachineTagList
        // Get Machine Tag List for a Particular Machine or Array of Machines
        // Created By: Siba Prasad Hota 
        // 11 June 2018 2:29 AM
        public function getalltags(){
            $machines = $this->input->get('machines');
            $myArray = explode(',', $machines);
            $result = $this->Apimodel->getCustomerMachineTagList($myArray);
            if ($result) {

                echo json_encode(array("status"=>'Success',"Msg"=>"tags Listed","data"=>$result));
            }else{
                echo json_encode(array("status"=>'Failed',"Msg"=>'No tags added',"data"=>array()));
            }
            
            
        }

    }
    


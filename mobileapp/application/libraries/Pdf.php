<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
require_once dirname(__FILE__) . '/tcpdf/mypdf.php';
class Pdf extends MYPDF
{
    function __construct()
    {
        parent::__construct();
    }
}
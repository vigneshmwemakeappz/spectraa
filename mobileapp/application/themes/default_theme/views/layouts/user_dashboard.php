<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
    $f_lang = get_front_language();
    $this->session->set_userdata('referred_from', base_url( $_SERVER["REQUEST_URI"]  ) );
 	$version = "v1.1";
 	$user_session_data = $this->session->userdata('user_data');
 	$user_session_data_id = $user_session_data['ID'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title><?php echo $meta_title; ?></title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url("assets/img/favicon.ico");?>"/>
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <!-- font Awesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">

    <!-- Main Styles CSS -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css');?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">

    <link href="<?php echo base_url('assets/css/owl.carousel.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/owl.theme.default.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-select.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/datepicker.min.css');?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var user_session_id = '<?php echo $user_session_data_id; ?>';
		var referred_from = '<?php echo $this->session->userdata('referred_from'); ?>';
	</script>
    <style type="text/css">
        .goog-te-banner-frame.skiptranslate {
    display: none !important;
    } 
body {
    top: 0px !important; 
    }
    </style>
</head>
<!-- aboutPage-body catPage-body clientPage-body foodPage-body -->
<body class="home-body <?php echo ( isset($body_class) )?$body_class:"";?>">
		<?php get_other_page_header_after_login(); ?>
	    <?php 
	     	echo $template['body']; 
		   	get_footer_section();
		    get_business_popup();
		    get_login_popup();
		    get_quick_reply_popup();
		   	get_sign_up_popup();
		   	get_forgot_password_popup();
		    get_new_password_popup();
		    get_subscribe_popup();
		    get_thank_popup();
		    get_subscribe_thank_popup();
		    get_feedback_popup();
            get_feedback_thank_popup();

		 ?>
<!-- jQuery -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-select.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/rating.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.scrollbar.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.vticker-min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/slick.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/rating.js');?>"></script>
<script src="<?php echo base_url('assets/js/scripts.js');?>"></script>
<script>
    $('[data-toggle="datepicker"]').datepicker({
        format: "dd M yyyy"
    });
</script>

<script>
	$(document).ready(function (e) {

        $('#f_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-danger');
                    $("#profile_error_msg").html('');
                return true;
            }
            else{
                e.preventDefault();
                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only alphabet in Name field.');
                return false;
            }
        });
        $('#l_name').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-danger');
                    $("#profile_error_msg").html('');
                return true;
            }
            else{
                e.preventDefault();
                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only alphabet in Name field.');
                return false;
            }

        });

        
         $("#p_number").keypress(function (e) {
            var qr_number = $('#p_number').val();
            if( qr_number.toString().length == 10 ) {
                return false;
            }else{
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ){
                    $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-success');
                    $("#profile_error_msg").addClass('alert alert-danger');
                    $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only number in phone number field.');
                   return false;
                }else{
                    $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-danger');
                    $("#profile_error_msg").html('');
                }     
            }
            
       });

          $('#profile_frm').on('submit',(function(e){
            e.preventDefault();
            is_valid = true;

            var Email = $( "#p_email" ).val();
            var f_name = $( "#f_name" ).val();
            var l_name = $( "#l_name" ).val();
            var p_number = $( "#p_number" ).val();
            var Gender = $( "#Gender" ).val();
            var DateOfBirth = $( "#dob" ).val();
            var submit_frm_type = $( "#submit_frm_type" ).val();


            if( submit_frm_type == 'frm' && f_name.toString().length == 0) {
                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>First name field is required.');
                is_valid = false;
            }else{
                if(  submit_frm_type == 'frm' &&  f_name.toString().length < 3 || f_name.toString().length > 25  ){
                   $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-success');
                    $("#profile_error_msg").addClass('alert alert-danger');
                    $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>please enter first name between 3 to 25 character.');
                    is_valid = false; 
                }
                    
            }

            if(  submit_frm_type == 'frm' &&  l_name.toString().length == 0) {
                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Last Name field is required.');
                is_valid = false;
            }else{
                if(  submit_frm_type == 'frm' &&  l_name.toString().length < 3 || l_name.toString().length > 25  ){
                   $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-success');
                    $("#profile_error_msg").addClass('alert alert-danger');
                    $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>please enter Last name between 3 to 25 character.');
                    is_valid = false; 
                }
            }
            if (  submit_frm_type == 'frm' &&  Email.toString().length == 0) {
                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Email field is required.');
                is_valid = false;
            } else {
                if (  submit_frm_type == 'frm' &&  !isValidEmailAddress(Email)) {
                    $("#profile_error_msg").removeClass('alert');
                    $("#profile_error_msg").removeClass('alert-success');
                    $("#profile_error_msg").addClass('alert alert-danger');
                    $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter valid Email .');
                    is_valid = false;
                }
            }
            
            if(  submit_frm_type == 'frm' &&  p_number.toString().length >= 1 && p_number.toString().length != 10 ) {

                $("#profile_error_msg").removeClass('alert');
                $("#profile_error_msg").removeClass('alert-success');
                $("#profile_error_msg").addClass('alert alert-danger');
                $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Mobile number should be 10 digit.');
                is_valid = false;
            }

            /*$("#profile_error_msg").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>please enter valid</div>');*/
            if( is_valid == true ){
             
                var formData = new FormData(this);
                $.ajax( {
                    type:'POST',
                    url: base_url+'users/profile/change_image',
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success:function(res){
                        var result = JSON.parse( res );
                        if( result.status == 1 ){
                           if( result.submit_frm_type == 'image' ){ 
                                $( "#submit_frm_type" ).val( 'frm' );
                                $( "#pro_img" ).attr( 'src', base_url+result.new_image );
                           }else{
                                if( result.redirect == 1 ){ 
                                    $("#profile_error_msg").removeClass('alert');
                                    $("#profile_error_msg").removeClass('alert-danger');
                                    $("#profile_error_msg").addClass('alert alert-success');
                                    $("#profile_error_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Profile details updated successfull.');
                                    setTimeout(function(){ 
                                        location.href = base_url+"users/profile";
                                    }, 1000);
                                }
                           }
                        }
                    },
                    error: function(data){
                        console.log("error");
                        console.log(data);
                    }
                });
            }
        }));
        
        $("#pro_image").on("change", function(){
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!( ( imagefile==match[0] ) || ( imagefile==match[1] ) || ( imagefile==match[2] ) ) ){
                $("#image_msg").html("<span style='color:red;''>please select only jpeg, jpg and png file</span>");
                return false;
            }else{
                $("#submit_frm_type").val( "image" );
                $("#profile_frm").submit();   
            }
             
        });
    });
    $( document ).ready( function( ){
	 	setTimeout( function(){ 
	 		fill_star();
	 		fill_star_d();
 		},1000);
           <?php 
        if( isset( $active_tab ) && $active_tab == 'my-ratings' ){
            ?>
            get_all_rating();     
            <?php 
        }
        if( isset( $active_tab ) && $active_tab == 'my-reviews' ){
        ?>
            get_all_review();
        <?php 
        } ?>
        

 		$('.ratings').on('starrr:change', function(e, value){
        	if( loggin_status == 0 ){
             	$( "#loginPop" ).modal( 'show' );
        	}else{
        		var con_id = $(this).data('contractid');
        		var total_rating = $('#r_val_'+con_id).val();
        		do_rating_d( con_id , value , total_rating );
        	}
        });

        $('.ratings_l').on('starrr:change', function(e, value){
        	if( loggin_status == 0 ){
             	$( "#loginPop" ).modal( 'show' );
        	}else{
        		var con_id = $(this).data('contractid');
        		var total_rating = $('#r_val_'+con_id).val();
        		do_rating( con_id , value , total_rating );
        	}
        });
    });

     function get_event( event_id ){

        if( event_id == 'all_events' ){
            location.href = base_url+"event";

        }else{
            
            location.href = base_url+"event/event_details/"+event_id;    
        }
        
    }
 

    function submit_search( ){
        var city_name = $("#city_name").val();
        var city_name = $("#area_name").val();
        var city_name = $("#client_cat_id").val();
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }

	function show_business(){

        $("#businessPop").modal('show');
        $("#ab_name").val('');
        $("#ab_companyname").val('');
        $("#ab_category").val('');
        $("#ab_number").val('');
        $("#ab_localty").val('');
        $("#ab_landline").val('');
        $("#ab_address").val('');
        $("#ab_landmark").val('');
        setTimeout( function(){ 
            get_categories_opt_html();
            get_location_opt_html();
        }, 1000);
    }

    function get_categories_opt_html( ){
        $.ajax({
            url: "<?php echo base_url('index/get_categories_opt_html/');?>",
            success: function( res ){
                var result = JSON.parse( res );
                var All_Category = result.cat_data;
                get_autofill_inputbox( 'ab_category', All_Category );
            }
        });
        return false;

    }
    function get_location_opt_html( ){
        $.ajax({
            url: "<?php echo base_url('index/get_location_opt_html/');?>",
            success: function( res ){
                var result = JSON.parse( res );
                var All_Localty = result.loc_data;
                get_autofill_inputbox_addbusiness( 'ab_localty', All_Localty );
                
            }
        });
        return false;
    }
            
 	function share_link( url ){
        if( loggin_status == 0 ){
            $( "#loginPop" ).modal( 'show' );
        }else{
            if( url ){
                window.open(url, '_blank');          
            }
        }
    }
    
     function show_map( lat , lon ){
        $( "#map-popup" ).modal( 'show' );
        $( "#map-box" ).html( '<iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='+lat+','+lon+'&hl=es;z=12&amp;output=embed" style=" display:block;border:0; position: relative; "  allowfullscreen > </iframe>' );
    }

    function fill_star_d(){

    	$('.ratings').each(function (event) {
		  	var rating = $(this).attr('rating');
			  for(var i = 0; (i < rating); i++){
		  		$(this).find('span').eq(i).removeClass('fa-thumbs-o-up');
		        $(this).find('span').eq(i).addClass('fa-thumbs-up');
			  }
		}); 
    }

	function fill_star(){
		$('.ratings_l').each(function (event) {
		  	var rating = $(this).attr('rating');
			  for(var i = 0; (i < rating); i++){
		  		$(this).find('span').eq(i).removeClass('fa-thumbs-o-up');
		        $(this).find('span').eq(i).addClass('fa-thumbs-up');
			  }
		}); 	
	}

	
	function do_rating_d( contract_id , ratings , total_rating){
		$.ajax({
            url: "<?php echo base_url('client/add_rating/');?>",
            type:'POST',
            data:{contractid:contract_id, rating:ratings },
            success: function( res ){
                var result = JSON.parse( res );
                if( result.status == 1 && result.add_status == 1  ){
                	var rsum = parseInt(total_rating)+1;
                	$(".r_"+contract_id ).html( rsum );
                	$("#r_d_"+contract_id ).html( rsum +' Reviews');
                	$("#r_val_"+contract_id ).val( rsum );
                }
            }
        });
        return false;
	}

	function do_rating( contract_id , ratings , total_rating){
		$.ajax({
            url: "<?php echo base_url('client/add_rating/');?>",
            type:'POST',
            data:{contractid:contract_id, rating:ratings },
            success: function( res ){
                var result = JSON.parse( res );
                if( result.status == 1 && result.add_status == 1  ){
                	var rsum = parseInt(total_rating)+1;
                	$("#r_"+contract_id ).html("("+rsum+")");
                	$("#r_val_"+contract_id ).val( rsum );
                }
            }
        });
        return false;
	}

    $(document).ready(function () {
        $('#secondary').slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            rows: 2,
            draggable: true,
            nextArrow: '<i class="fa fa-angle-right"></i>',
            prevArrow: '<i class="fa fa-angle-left"></i>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        infinite: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        infinite: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true
                    }
                }
            ]
        });


        $(window).load(function () {

            $('#secondary .foodItems').each(function (i) {
                var item = $('<div class="item"></div>');
                var itemDiv = $(this);
                var title = $(this).find('a').attr("title");
                var title2 = $(this).find('a').attr("alt");

                item.attr("title", title);
                item.attr("alt", title2);
                $(itemDiv.html()).appendTo(item);
                item.appendTo('.getInsight .carousel-inner');
                if (i == 0) {
                    item.addClass('active');
                }
            });

        });

        /* activate the carousel */
        $('#modaCarousel').carousel({interval: false});

        /* change modal title when slide changes */
        $('#modaCarousel').on('slid.bs.carousel', function () {
            $('.SlidDetail p').html($(this).find('.active').attr("title"));
            $('.SlidDetail h3').html($(this).find('.active').attr("alt"));
        });

        /* when clicking a thumbnail */
        $('#secondary .foodItems a').click(function () {
            var idx = $(this).parent('.foodItems').index();
            var id = parseInt(idx);
            $('#myModal').modal('show'); // show the modal
            $('#modaCarousel').carousel(id); // slide carousel to selected

        });

    });
</script>
		<script type="text/javascript">
		    $(document).ready(function(){
                
	        	get_all_city();
		        setTimeout( function(){ 
		            set_page_language( <?php echo $this->session->userdata('lang_id');?> );
		        }, 3000);
                

                $('#forgotpasswordform').submit(function(e) {
            e.preventDefault();
            $('#forgot_msg').html("");
            var is_valid = true;        
            var emailid = $.trim($('#forgotemail').val());        
            if (emailid.toString().length == 0) {   
                var ptag = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Register email required</div>';
                $('#forgot_msg').append(ptag);
                is_valid = false;
            } else {
                if (!isValidEmailAddress(emailid)) {                
                    var ptag = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please enter valid email</div>'; 
                    $('#forgot_msg').append(ptag);
                    is_valid = false;
                }
            } 

            if (is_valid) {
                $.ajax({
                    url: base_url + 'sendemail/forgot_password',
                    type: 'POST',
                    data: {                   
                        emailid: emailid
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == "ok") {
                            $('#forgot_msg').html("");                        
                            var ptag = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>'; 
                            $('#forgot_msg').append(ptag);                        
                            setTimeout(function() {                           
                                $('#forgotemail').val('');
                                $('#forgot_msg').html("");
                                $(".close").trigger("click");
                                $("#thankPop").trigger("click");
                            }, 5000);
                        } else {
                            $('#forgot_msg').html("");
                            var ptag = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> ' + data.msg + '</div>'; 
                            $('#forgot_msg').append(ptag);
                        }
                    }
                });
            }
        });
		        $('.translation-icons li a').click(function(e) {
		            e.preventDefault();
		            $( "#google_translate_element" ).show();
		            var placement = $(this).data('placement');
		            var langname = $(this).data('langname');
		            set_page_language( placement );
		            setTimeout( function(){ 
		                location.href = "<?php echo $this->session->userdata('referred_from')."?lang=";?>"+langname;
		            }, 1000);
		            return false;
		        } );

                 
                
                $( "#subscribe_frm" ).submit(function( e ) {
                    e.preventDefault();
                    var s_email = $( "#s_email" ).val();
                    $.ajax({
                        url: base_url+'/index/add_subscribe',
                        type:'POST',
                        data:{email_id:s_email},
                        success: function( res ){
                            var result = JSON.parse( res );
                            if( result.status == 1 ){
                                $('#subscribeUs').modal('hide');
                                $('#s_email').val();
                                $('#subscribeThankPop').modal('show');
                            }else{
                                
                                $('#s_msg').html('<div class="alert alert-danger fade in alert-dismissable">Something want wrong please try again.<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>');
                                
                            }
                        }
                    });
                    return false;  
                });

		        $( "#login_frm" ).submit(function( e ) {
		            e.preventDefault();
		            var l_username = $( "#email_id" ).val();
		            var l_password = $( "#password_login" ).val();
		            if( l_username == ''  || l_password == '' ){
		                $("#login_msg").addClass( 'alert alert-danger' );
		                $("#login_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Required User ID and Password fields' );
		                setTimeout( function(){ 
		                    $("#login_msg").removeClass('alert');
		                    $("#login_msg").removeClass('alert-success');
		                    $("#login_msg").removeClass('alert-danger');
		                    $("#login_msg").html('');
		                },1000 );
		            }else{
		                $.ajax({
		                    url: "<?php echo base_url('index/do_login/');?>",
		                    type:'POST',
		                    data:{email_id:l_username, password:l_password },
		                    success: function( res ){
		                        var result = JSON.parse( res );
		                        if( result.status == 1 ){
		                            $("#login_msg").removeClass( 'alert' );
		                            $("#login_msg").removeClass( 'alert-danger' );
		                            $("#login_msg").addClass( 'alert alert-success' );
		                            $("#login_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
		                                setTimeout( function(){ 
		                                    location.href = "<?php echo $this->session->userdata('referred_from');?>";
		                                }, 1000);

		                        }else{
		                            $("#login_msg").removeClass( 'alert' );
		                            $("#login_msg").removeClass( 'alert-success' );
		                            $("#login_msg").addClass( 'alert alert-danger' );
		                            $("#login_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
		                        }
		                        setTimeout( function(){ 
			                    $("#login_msg").removeClass('alert');
			                    $("#login_msg").removeClass('alert-success');
			                    $("#login_msg").removeClass('alert-danger');
			                    $("#login_msg").html('');
			                },1000 );
		                    }

		                });
		                return false;   
		            }
		            return false;
		        });
		        
		        $('#ab_name').keypress(function (e) {
	                    var regex = new RegExp("^[a-zA-Z ]+$");
	                    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	                    var ab_name = $("#ab_name").val();
	                    if( ab_name.toString().length == 25 ){
	                          return false;
	                    }else{
	                        if (regex.test(str)) {
	                            $("#add_business_msg").removeClass('alert');
	                            $("#add_business_msg").removeClass('alert-danger');
	                            $("#add_business_msg").html('');
	                            return true;
	                        }
	                        else{
	                            e.preventDefault();
	                            $("#add_business_msg").removeClass('alert');
	                            $("#add_business_msg").removeClass('alert-success');
	                            $("#add_business_msg").addClass('alert alert-danger');
	                            $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only alphabet in Name field.');
	                            return false;
	                        }     
	                    }
	                });

        $("#ab_area_code").keypress(function (e) {
            
            var qr_number = $('#ab_area_code').val();
            if( qr_number.toString().length == 6 ){
                return false;
            }else{
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ){
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-success');
                    $("#add_business_msg").addClass('alert alert-danger');
                    $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only number in Area Code field.');
                   return false;
                }else{
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-danger');
                    $("#add_business_msg").html('');
                }      
            }
            
            
       });

        $("#ab_landline").keypress(function (e) {
            var qr_number = $('#ab_landline').val();

            
            if( qr_number.toString().length == 8 ){
                return false;
            }else{
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ){
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-success');
                    $("#add_business_msg").addClass('alert alert-danger');
                    $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only number in Landline Number field.');
                   return false;
                }else{
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-danger');
                    $("#add_business_msg").html('');
                }      
            }
            
            
       });

        $("#ab_number").keypress(function (e) {
            var qr_number = $('#ab_number').val();
            if( qr_number.toString().length == 10 ) {
                return false;
            }else{
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) ){
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-success');
                    $("#add_business_msg").addClass('alert alert-danger');
                    $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter only number in phone number field.');
                   return false;
                }else{
                    $("#add_business_msg").removeClass('alert');
                    $("#add_business_msg").removeClass('alert-danger');
                    $("#add_business_msg").html('');
                }     
            }
            
       });
		        
                $( "#add_business_frm" ).submit(function( e ) {
                    e.preventDefault();
                    var is_valid = true; 
                    //var frm_data = $("#add_business_frm").serialize();
                    var ab_name = $( "#ab_name" ).val();
                    var ab_companyname = $( "#ab_companyname" ).val();
                    var ab_category = $( "#ab_category" ).val();
                    var ab_number = $( "#ab_number" ).val();
                    var ab_localty = $( "#ab_localty" ).val();
                    var ab_landline = $( "#ab_landline" ).val();
                    var ab_address = $( "#ab_address" ).val();
                    var ab_landmark = $( "#ab_landmark" ).val();
                    var ab_owner = $( "#ab_owner" ).val();
                    var ab_email = $( "#ab_email" ).val();
                    var ab_area_code = $( "#ab_area_code" ).val();
if( ab_area_code.toString().length != 0 && ( ab_area_code.toString().length < 3 || ab_area_code.toString().length > 6 )  ){

                $("#add_business_msg").removeClass('alert');
                $("#add_business_msg").removeClass('alert-success');
                $("#add_business_msg").addClass('alert alert-danger');
                $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Area Code field must be at least 3 to 6 character in length.');
                return false;
            }

            if( ab_landline.toString().length != 0  && ab_landline.toString().length < 5  ){
                $("#add_business_msg").removeClass('alert');
                $("#add_business_msg").removeClass('alert-success');
                $("#add_business_msg").addClass('alert alert-danger');
                $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter minimum 5 digits number in Landline Number field.');
                return false;
            }
            
                    if (ab_email.toString().length == 0) {
                        $("#add_business_msg").removeClass('alert');
                        $("#add_business_msg").removeClass('alert-success');
                        $("#add_business_msg").addClass('alert alert-danger');
                        $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Email field is required.');
                        is_valid = false;
                    } else {
                        if (!isValidEmailAddress(ab_email)) {
                            $("#add_business_msg").removeClass('alert');
                            $("#add_business_msg").removeClass('alert-success');
                            $("#add_business_msg").addClass('alert alert-danger');
                            $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Enter valid Email .');
                            is_valid = false;
                        }
                    }

                    if( ab_landline.toString().length >= 0 && ab_area_code.toString().length == 0 ){
                        $("#add_business_msg").removeClass('alert');
                        $("#add_business_msg").removeClass('alert-success');
                        $("#add_business_msg").addClass('alert alert-danger');
                        $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Area code field is required.');
                        is_valid = false;
                    }
            
                    if( ab_name.toString().length == 0 ){
                        $("#add_business_msg").removeClass('alert');
                        $("#add_business_msg").removeClass('alert-success');
                        $("#add_business_msg").addClass('alert alert-danger');
                        $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Name field is required.');
                        is_valid = false;
                    }else{
	                if(ab_name.toString().length < 3 || ab_name.toString().length > 25 ) {
	                    $("#add_business_msg").removeClass('alert');
	                    $("#add_business_msg").removeClass('alert-success');
	                    $("#add_business_msg").addClass('alert alert-danger');
	                    $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Name field must be at least 3 to 50 character in length.');
	                    is_valid = false;
	                }
	            }
                    
                    if (ab_companyname.toString().length == 0) {
                        $("#add_business_msg").removeClass('alert');
                        $("#add_business_msg").removeClass('alert-success');
                        $("#add_business_msg").addClass('alert alert-danger');
                        $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Email field is required.');
                        is_valid = false;
                    }  
                    
                    
                    if( ab_number.toString().length >= 1 && ab_number.toString().length != 10 ) {

                        $("#add_business_msg").removeClass('alert');
                        $("#add_business_msg").removeClass('alert-success');
                        $("#add_business_msg").addClass('alert alert-danger');
                        $("#add_business_msg").html('<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Phone number should be 10 digit.');
                        is_valid = false;
                    }
		setTimeout( function(){ 
	                $("#add_business_msg").removeClass('alert');
	                $("#add_business_msg").removeClass('alert-success');
	                $("#add_business_msg").removeClass('alert-danger');
	                $("#add_business_msg").html('');
	            },1000);
                    if( is_valid ){
                        $.ajax({
                            url: "<?php echo base_url('index/add_business/');?>",
                            type:'POST',
                            data:{
                                ab_name:ab_name,
                                ab_companyname:ab_companyname,
                                ab_category:ab_category,
                                ab_number:ab_number,
                                ab_localty:ab_localty,
                                ab_landline:ab_landline,
                                ab_address:ab_address,
                                ab_landmark:ab_landmark,
                                ab_owner:ab_owner,
                                ab_area_code:ab_area_code,
                        ab_email:ab_email
                            },
                            success: function( res ){
                                var result = JSON.parse( res );
                                if( result.status == 1 ){
                                    $("#add_business_msg").removeClass( 'alert' );
                                    $("#add_business_msg").removeClass( 'alert-danger' );
                                    $("#add_business_msg").addClass( 'alert alert-success' );
                                    $("#add_business_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
                                    setTimeout( function(){ 
                                        location.href = "<?php echo $this->session->userdata('referred_from');?>";
                                    }, 1000);

                                }else{
                                    $("#add_business_msg").removeClass( 'alert' );
                                    $("#add_business_msg").removeClass( 'alert-success' );
                                    $("#add_business_msg").addClass( 'alert alert-danger' );
                                    $("#add_business_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
                                }
                                setTimeout( function(){ 
                            $("#add_business_msg").removeClass('alert');
                            $("#add_business_msg").removeClass('alert-success');
                            $("#add_business_msg").removeClass('alert-danger');
                            $("#add_business_msg").html('');
                        },1000);
                            }

                        });
                    }
                    return false;
                });
		        
		        $( "#review_frm" ).submit(function( e ) {
		            e.preventDefault();
		            var frm_data = $("#review_frm").serialize();
		            $.ajax({
		                url: "<?php echo base_url('client/add_review/');?>",
		                type:'POST',
		                data:frm_data,
		                success: function( res ){
		                    var result = JSON.parse( res );
		                    if( result.status == 1 ){
		                        $("#review_msg").removeClass( 'alert' );
		                        $("#review_msg").removeClass( 'alert-danger' );
		                        $("#review_msg").addClass( 'alert alert-success' );
		                        $("#review_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
		                            setTimeout( function(){ 
		                                location.href = "<?php echo $this->session->userdata('referred_from');?>";
		                            }, 3000 );
		                    }else{
		                        $("#review_msg").removeClass( 'alert' );
		                        $("#review_msg").removeClass( 'alert-success' );
		                        $("#review_msg").addClass( 'alert alert-danger' );
		                        $("#review_msg").html( '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+result.msg );
		                    }
		                }
		            });
		            return false;
		        });
                //uploader event
                
		    }); 
  
	     	function chk_login_status(){
	     		if( loggin_status == 0 ){
             		$( "#loginPop" ).modal( 'show' );
        		}
	     	}
		     
		    

		    function do_follow_listing( c_id ){
		    	if( loggin_status == 0 ){
             		$( "#loginPop" ).modal( 'show' );
        		}else{

        			var f_status = $("#f_status_"+c_id).val();
			        $.ajax({
			            url: "<?php echo base_url('search/do_follow_unfollow/');?>",
			            type:'POST',
			            data:{client_id:c_id,type:f_status },
			            success: function( res ){
			            	
			            	if( res == 1  && f_status == 'follow' ){

			            		$( "#f_icon_"+c_id ).html( 'unfollow' );
								$( "#f_icon_"+c_id ).addClass( 'chnge_color' );
			            		var total_f = $( "#f_val_"+c_id ).val();
			            		var fsum = parseInt(total_f)+1;
			            		$( "#f_val_"+c_id ).val( fsum );
			            		$( "#f_"+c_id ).html('( '+ fsum +' )' );
			            		$("#f_status_"+c_id).val( 'unfollow' );
			            	}
			            	else if( res == 1 ){

			            		$( "#f_icon_"+c_id ).html( 'follow' );
								$( "#f_icon_"+c_id ).removeClass( 'chnge_color' );
			            		var total_f = $( "#f_val_"+c_id ).val();
			            		var fsum = parseInt(total_f)-1;
			            		$( "#f_val_"+c_id ).val( fsum );
			            		$( "#f_"+c_id ).html('( '+ fsum +' )' );
			            		$("#f_status_"+c_id).val( 'follow' );
			            	} 
			            }
			        });
		        	return false;
		       	}
		    }

		    function do_like_listing( c_id ){
		    	if( loggin_status == 0 ){
             		$( "#loginPop" ).modal( 'show' );
        		}else{

        			var l_status = $("#l_status_"+c_id).val();
			        $.ajax({
			            url: "<?php echo base_url('search/do_like_dislike/');?>",
			            type:'POST',
			            data:{client_id:c_id,type:l_status },
			            success: function( res ){
			            	
			            	if( res == 1  && l_status == 'like' ){

			            		


			            		$( "#l_icon_"+c_id ).attr('src',base_url+"assets/img/ok-icon-o.svg" );

			            		var total_l = $( "#l_val_"+c_id ).val();
			            		var lsum = parseInt(total_l)+1;
			            		$( "#l_val_"+c_id ).val( lsum );
			            		$( "#l_"+c_id ).html('( '+ lsum +' )' );
			            		$("#l_status_"+c_id).val( 'dislike' );
			            	}
			            	else if( res == 1 ){

			            		$( "#l_icon_"+c_id ).attr('src',base_url+"assets/img/ok-icon.svg" );
			            		var total_l = $( "#l_val_"+c_id ).val();
			            		var lsum = parseInt(total_l)-1;
			            		$( "#l_val_"+c_id ).val( lsum );
			            		$( "#l_"+c_id ).html('( '+ lsum +' )' );
			            		$("#l_status_"+c_id).val( 'like' );
			            	} 
			            }
			        });
		        	return false;
		       	}
		    }

		    function is_like_listing( c_id, l_type , c_o_id ){
		    	if( loggin_status == 0 ){
             		$( "#loginPop" ).modal( 'show' );
        		}else{
			        $.ajax({
			            url: "<?php echo base_url('client/like_client/');?>",
			            type:'POST',
			            data:{client_id:c_id,type:l_type },
			            success: function( res ){

			            	if( ( res == 1 || res == 2 ) && l_type == 'like'  ){

			            		var total_l = $( "#l_val_"+c_id ).val();
			            		var lsum = parseInt(total_l)+1;
			            		$( "#l_val_"+c_id ).val( lsum );
			            		$( "#l_"+c_id ).html('( '+ lsum +' )' );

			            		$( "#like_icon_"+c_o_id ).attr('src',base_url+"assets/img/ok-icon-o.svg" );

			            		$( "#like_icon_"+c_o_id ).attr('src',base_url+"assets/img/ok-icon-o.svg" );
			            	}
			            	if( ( res == 1 || res == 2 ) && l_type == 'follow'  ){
			            		$( "#follow_icon_"+c_o_id ).html( 'followed' );
								$( "#follow_icon_"+c_o_id ).addClass( 'chnge_color' );
			            	}
			            }
			        });
		        	return false;
		       }
		    }

		    function is_like( c_id, l_type ){

		    	if( l_type == 'like' ){
		    		var t_like_status = $( "#like_status" ).val();		
		    	}
		    	if( l_type == 'follow' ){
		    		var t_like_status = $( "#follow_status" ).val();		
		    	}
		    	

		        $.ajax({
		            url: "<?php echo base_url('client/like/');?>",
		            type:'POST',
		            data:{client_id:c_id,like_status:t_like_status,type:l_type },
		            success: function( res ){
		            	var result = JSON.parse( res );

		            	if( l_type == 'like' ){
		            		if( result.status == 1 ){
			                	$( "#like_us" ).html( "Liked" );
			                	$( "#like_status" ).val( "Liked" );
			                }
			                $( "#thumbs_up" ).html( result.thumbs_up );	
		            	}

		                if( l_type == 'follow' && result.status == 1 ){
		                	
		                	$( "#follow_us" ).html( "Followed" );
		                	$( "#follow_status" ).val( "followed" );
		                }
		            }
		        });
		        return false;
		    }

		    function get_last_just_in(){
		        var last_just_in_id = $( "#just_in_id" ).val();
		        $.ajax({
		            url: "<?php echo base_url('index/get_last_just_in/');?>",
		            type:'POST',
		            data:{last_id:last_just_in_id},
		            success: function( res ){
		                var result = JSON.parse( res );
		                if( result.status == 1 ){
		                    
		                    $("#just_in_ul").prepend( result.justin_str );
		                    $("#just_in_id").val( result.last_rec_id );
		                }
		            }
		        });
		        return false;
		    } 

		    function set_page_language( lang_id ){
		        var $frame = $('.goog-te-menu-frame:first');
		        var langs = $('.goog-te-menu-frame:first').contents().find('a span.text');
		        langs.eq( lang_id ).click();
		        $( "#google_translate_element" ).hide();
		    }

            function get_all_city( ){
                $.ajax({
                    url: "<?php echo base_url('index/get_all_city/');?>",
                    type:'POST',
                    data:{},
                    success: function( res ) {
                        var result = JSON.parse( res );
                        $( "#city_name" ).val( result.default_city );
                        var get_city_name = $( '#city_name' ).val();
                        if( get_city_name == '' ){
                            $( "#city_name" ).val( result.default_city );
                            $( ".c_city" ).html( result.default_city );
                        }else{
                            $( ".c_city" ).html( get_city_name );
                        }
                        setTimeout( function(){ 
                            get_area_name();
                        }, 100);
                        var Auto_Fill_City_Data = result.city_data;
                        get_autofill_inputbox( 'city_name',Auto_Fill_City_Data );
                    }
                });
                return false;
            }


		    function get_area_name( ){
                var city_name_val = $("#city_name").val();
                if( city_name_val != "" ){
                    $.ajax({
                        url: "<?php echo base_url('index/get_area_name/');?>",
                        type:'POST',
                        data:{city_name:city_name_val},
                        success: function( res ) {
                            var result = JSON.parse( res );
                            setTimeout(function(){ 
                                fill_auto_input();
                            }, 500);
                            var Auto_Fill_Areas = result.area_data;
                            //$( "#area_name" ).val( result.default_area );
                            get_autofill_inputbox( 'area_name',Auto_Fill_Areas );
                        }
                    });
                }
                return false;
            }

		    function fill_auto_input( ){
                 $('#client_cat_id').keyup(function() {
        var searchval= $(this).val();
        if(searchval .length > 0){
       
        var city_name_val = $("#city_name").val();
        var area_name_val = $("#area_name").val();
        $.ajax({
            url: "<?php echo base_url('index/fill_auto_input/');?>",
            type:'POST',
            data:{
                city_name:city_name_val,
                searchval:searchval,
                area_name:area_name_val
            },
            success: function( res ) {
           //alert(res);
                var result = JSON.parse( res );
                var Auto_Fill_Client_Category = result.autocomplete;
                get_autofill_inputbox_client( 'client_cat_id',Auto_Fill_Client_Category );
            }
        });
        }
        });
        return false;
            } 
		    
		    function get_default_areaname(){
		        var default_city_id = $("#city_id").val();
		        $.ajax({
		            url: "<?php echo base_url('index/get_area/');?>",
		            type:'POST',
		            data:{city_id:default_city_id},
		            success: function( res ) {
		                var result = JSON.parse( res );
		                $("#area_menu").html( result.area_str );
		                $("#default_area").html( '<span class="label-icon">'+result.default_area_str+'</span>' );
		                $("#default_area").attr( 'data-default-area', result.default_area_id );
		                var availableTags = result.autofill_options;
		                $(".autocomplete").autocomplete({
		                    source: availableTags
		                });
		            }
		        });
		        return false;
		    } 

		    function get_autofill_inputbox_client( fieldid,fill_data ){

                var _this = $("#"+fieldid),
                    _data = _this.data(),
                    _hidden_field = $('#' + _data.hidden_field_id);
                    _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
                        .parent('.form-group').addClass('has-feedback');
                        var feedback_icon = _this.next('.bs-autocomplete-feedback');
                        feedback_icon.hide();
                        _this.autocomplete({
                        minLength: 1,
                        autoFocus: false,
                        source: function(request, response) {
                            var _regexp = new RegExp(request.term, 'i');
                            var data = fill_data.filter(function(item) {
                                return item.cityName.match(_regexp);
                            });
                            response(data);
                        },  
                        //
                        search: function() {
                            feedback_icon.show();
                            _hidden_field.val('');
                        },
                        response: function() {
                            feedback_icon.hide();
                        },
                        focus: function(event, ui) {
                            _this.val(ui.item[_data.item_label]);
                            event.preventDefault();
                        },
                        select: function(event, ui) {
                            _this.val(ui.item[_data.item_label]);
                            _hidden_field.val(ui.item[_data.item_id]);
                            var city_name = $('#city_name').val( );
                            var area_name =  $('#area_name').val( );
                            location.href = ui.item[_data.item_id]+'/?city_name='+city_name+'&area_name='+area_name;
                            event.preventDefault();
                        }
                    }).data('ui-autocomplete')._renderItem = function(ul, item) {
                    return $('<li></li>')
                        .data("item.autocomplete", item)
                        .append('<a>' + item[_data.item_label] + '</a>')
                        .appendTo(ul);
                    };
            }

            function get_autofill_inputbox( fieldid,fill_data ){

                var _this = $("#"+fieldid),
                    _data = _this.data(),
                    _hidden_field = $('#' + _data.hidden_field_id);
                    _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
                        .parent('.form-group').addClass('has-feedback');
                        var feedback_icon = _this.next('.bs-autocomplete-feedback');
                        feedback_icon.hide();
                        _this.autocomplete({
                        minLength: 1,
                        autoFocus: false,
                        source: function(request, response) {
                            var _regexp = new RegExp(request.term, 'i');
                            var data = fill_data.filter(function(item) {
                                return item.cityName.match(_regexp);
                            });
                            response(data);
                        },
                        search: function() {
                            feedback_icon.show();
                            _hidden_field.val('');
                        },
                        response: function() {
                            feedback_icon.hide();
                        },
                        focus: function(event, ui) {
                            _this.val(ui.item[_data.item_label]);
                            event.preventDefault();
                        },
                        select: function(event, ui) {
                            _this.val(ui.item[_data.item_label]);
                            _hidden_field.val(ui.item[_data.item_id]);
                            if( fieldid == 'city_name' ){
                                var city_name_val = $("#city_name").val();
                                $( ".c_city" ).html( city_name_val );     
                            }
                            event.preventDefault();
                        }
                    }).data('ui-autocomplete')._renderItem = function(ul, item) {
                    return $('<li></li>')
                        .data("item.autocomplete", item)
                        .append('<a>' + item[_data.item_label] + '</a>')
                        .appendTo(ul);
                    };
            }

            /*function for user dashboard theme */
            
            function get_all_place( ){
                $.ajax({
                    url: base_url+'users/profile/get_all_place/',
                    type:'POST',
                    data:{
                        user_id:user_session_id
                    },
                    success: function( res ) {
                        var result = JSON.parse( res );
                        if( result.status == 1 ){
                        	$('#myPlace').html( result.place_str );	
                        }else{
                        	$('#myPlace').html( result.msg );	
                        }
                    }
                });
                return false;
            } 
            function get_all_rating( ){
                $.ajax({
                    url: base_url+'users/profile/get_all_rating/',
                    type:'POST',
                    data:{
                        user_id:user_session_id
                    },
                    success: function( res ) {
                        var result = JSON.parse( res );
                        
                        if( result.status == 1 ){
                        	$('#myRatings').html( result.rating_str );	
                        }else{
                        	$('#myRatings').html( result.msg );	
                        }
                    }
                });
                return false;
            } 
            function get_all_review( ){
                $.ajax({
                    url: base_url+'users/profile/get_all_review/',
                    type:'POST',
                    data:{
                        user_id:user_session_id
                    },
                    success: function( res ) {
                        var result = JSON.parse( res );
                        if( result.status == 1 ){
                        	$('#myReviews').html( result.reviews_str );	
                        }else{
                        	$('#myReviews').html( result.msg );	
                        }
                    }
                });
                return false;
            }
            
             $("#client_cat_id").keypress(function(e) {
    if(e.which == 13) {
     var company=$(this).val();
      var  city=$("#city_name").val();
        var area=$("#area_name").val(); 
       // alert(city);
        window.location.href = "<?php echo base_url();?>search/"+company+"/?city_name="+city+"&area_name="+area;
    $("#go").click();
    }
});
$(".trendcat1").click(function () {
    var id = $(this).attr("id");
    //alert(id);
     var  getcat=encodeURI($("#getcat1"+id).val());
     var  city=$("#city_name").val();
      window.location.href = "<?php echo base_url();?>search/"+getcat+"/?city_name="+city;
    $("#go").click();
  
});
		</script>
	</body>
</body>
</html>
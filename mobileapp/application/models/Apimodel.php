<?php
    class Apimodel extends CI_Model {
        function __construct(){
            parent::__construct();
        }
        
        public  function GetHash($qtd) {
            //Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
            $Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
            $QuantidadeCaracteres = strlen($Caracteres);
            $QuantidadeCaracteres--;
            $Hash=NULL;
            for($x=1;$x<=$qtd;$x++){
                $Posicao = rand(0,$QuantidadeCaracteres);
                $Hash .= substr($Caracteres,$Posicao,1);
            }
            return $Hash;
        }
        
        
        public function checkAdminlogin($data){
            $this->db->select('*');
            $this->db->from('spectraa_admin');
            $this->db->where($data);
            $query = $this->db->get();
            if ( $query->num_rows() > 0 )  {
                $result = $query->row_array();
                foreach ($query->result() as $row){
                    $session_data = array(
                                          'userid'     => $row->Admin_Name,
                                          'Emp_ID'     => $row->Admin_EmpID,
                                          'Name'     => $row->Admin_Name,
                                          'Role'     => $row->Admin_RoleID
                                          );
                    
                }
                
            }
            return $session_data;
        }
        
        
        public function checkCustomerlogin($data){
            $this->db->select('*');
            $this->db->from('spectraa_customers');
            $this->db->where($data);
            $query = $this->db->get();
            if ( $query->num_rows() > 0 )  {
                $result = $query->row_array();
                foreach ($query->result() as $row){
                
                	$customerID = $row->Cust_ID;
                	$this->db->select('*');
               	 	$this->db->from('spectraa_devaccount');
                	$this->db->where("Cust_ID",$customerID);
                	$devquery = $this->db->get();
                	$devresult = $devquery->row_array();
                	
                    $customer_data = array(
                                          'CustomerID'  => $row->Cust_ID,
                                          'Name'        => $row->Cust_Name,
                                          'Company'     => $row->Cust_Company,
                                          'City'        => $row->Cust_City,
                                          'State'       => $row->Cust_State,
                                          'Address'     => $row->Cust_SiteAddress,
                                          'Mobile'      => $row->Cust_Mob,
                                          'isActive'    => $row->Cust_AcStatus,
                                          'devAccount'  => $devresult
                                          );
                    
                }
                
            }
            return $customer_data;
        }
        
        
        public function getCustomerMachineList($customerID){
            $this->db->select('Machine_ID, Machine_Name, Machine_Type');
            $this->db->from('spectraa_machines');
            $this->db->where("Machine_CustomerID",$customerID);
            $this->db->order_by("Machine_Name"); 
            $query = $this->db->get();
            if ( $query->num_rows() > 0 )  {
                $result = $query->row_array();
                $mType = "XYZ";
                $counter = 0;
                $finalData = array();
                foreach ($query->result() as $row){
                
                    $machine_data = array(
                                           'machineID'   => $row->Machine_ID,
                                           'machineName' => $row->Machine_Name
                                           );
                                           
                    if($mType == $row->Machine_Type){
                    	$newInternalData = $finalData[$counter-1]["Value"];
                    	$newInternalData[] = $machine_data;
                    	$finalData[$counter-1]["Value"] = $newInternalData;

                    }else{
                      	$mType = $row->Machine_Type;
                      	$internalData= array();                  
                      	$internalData[] = $machine_data;                     
                      	$finalData[] = array("Type"=>$mType,"Value"=>$internalData);
                      	$counter++;
                    }                        
                }
                
            }
            return $finalData;
        }
        


        public function getCustomerMachineTagList($machineID){
            $this->db->select('Tag_ID, Tag_Name,Tag_MachineID');
            $this->db->from('spectraa_tags');
            $this->db->where_in("Tag_MachineID",$machineID);
            $this->db->where('Tag_Type !=', 1);
            $this->db->order_by("Tag_MachineID"); 
            $query = $this->db->get();
            if ( $query->num_rows() > 0 )  {
                $result = $query->row_array();
                $mID = "XYZ";
                $counter = 0;
                $finalData = array();
                
                foreach ($query->result() as $row){
                 $machine_data = array(
                                           'tagID'     => $row->Tag_ID,
                                           'tagName'   => $row->Tag_Name
                                           );
                
                 if($mID == $row->Tag_MachineID){
                    $newInternalData = $finalData[$counter-1]["Value"];
                    $newInternalData[] = $machine_data;
                    $finalData[$counter-1]["Value"] = $newInternalData;

                  }else{
                      $mID = $row->Tag_MachineID;
                      $internalData= array();
                     
                      $internalData[] = $machine_data;                     
                      $finalData[] = array("machineID"=>$mID,"Value"=>$internalData);
                      $counter++;
                  } 
                }               
            }
            return $finalData;
        }
    }
    


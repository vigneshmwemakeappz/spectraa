<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//check admin is login or not 
function chk_user_login( $chk_type = '' ) {
    $ci =& get_instance();
    $user_data  = $ci->session->userdata( 'user_data' );
    if( $chk_type == 'admin'  ){
        if( !count( $user_data ) ){
            redirect( base_url('admin/') ); 
        }
    }else{
        if( !count( $user_data ) ){
            redirect( base_url() );
        }
    }
    return $user_data;
}

function get_front_language() {
    $ci =& get_instance();
    $ci->load->helper('language');
    $site_lang =  'english';

    $get_data = $ci->input->get();
    $ci->session->set_userdata( 'lang','en' );
    if( isset( $get_data['lang'] ) && $get_data['lang'] ){
        $ci->session->set_userdata( 'lang', $get_data['lang'] );
    }

    $lang = $ci->session->userdata('lang');
    if( $lang == 'kn' ){
        $site_lang =  'kannada';
    }
    $language_keys =  array();
    $ci->lang->load('front',$site_lang);
    $language_keys = $ci->lang->language;
    return $language_keys; 
}

function chk_lang( $lang_arr =  array(), $lang_key = "" ){

    if( isset( $lang_arr[$lang_key] )  ){
        echo $lang_arr[$lang_key];
    }else{
        echo $lang_key;
    }
}

function datetimeDiff($dt1, $dt2){

    $result =  array(
        'days' => 0, 
        'hours' => 0, 
        'minutes' => 0, 
        'seconds' => 0, 
    ); 
    $seconds = strtotime( $dt1 ) - strtotime( $dt2 );
    $days    = floor( $seconds / 86400 );
    $hours   = floor( ( $seconds - ($days * 86400 ) ) / 3600 );
    $minutes = floor( ( $seconds - ($days * 86400 ) - ( $hours * 3600 ) )/60 );
    $seconds = floor( ( $seconds - ($days * 86400 ) - ( $hours * 3600 ) - ( $minutes*60 ) ) );

    $diff_str = "";
    if( $days >= 1  ){
        $diff_str .= $days." Days ";
    }
    if(  $hours >= 1  ){
        $diff_str .= $hours." Hours ";
    }
    if( $days <= 0 && $minutes >= 1  ){
        $diff_str .= $minutes." Minutes ";
    }   
    if( $minutes <= 0 && $days <= 0 && $seconds >= 1  ){
        $diff_str .= $seconds." Seconds ";
    } 

    $result['diff_str'] = ( trim($diff_str) != "" )?$diff_str."ago":"";
    $result['days'] = $days;
    $result['hours'] = $hours;
    $result['minutes'] = $minutes;
    $result['seconds'] = $seconds;
    return $result;

}
 
<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed' );

//pre print function
function _P( $var ) {

    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

//table name tbl_ prefixed
function tbl( $var ){
    return "tbl_" . trim( $var );
}

function do_file_upload($upload_path = "", $file_name = "") {

    $CI = & get_instance();
    $config['upload_path'] = $upload_path; //$upload_path; //'./assets/upload/gift_product/'
    $config['allowed_types'] = 'gif|jpg|png';
    $ext = explode('.', $_FILES[$file_name]['name']);
    
    $new_name = time() . "." . $ext[1];
    $config['file_name'] = $new_name;
    $CI->load->library('upload', $config);
    if (!$CI->upload->do_upload($file_name)) {
        $result = "";
        //echo $CI->upload->display_errors($file_name);
    } else {
        $result = $new_name;
    }
    return $result;
}


function get_active_stars_r( $star_value = 0  ) {


    $star_str = '<div class="starssss">';
    for($i=1; $i <= 5 ; $i++) { 
        if( $i <= $star_value ){
            $star_str .= '<span class="fa fa-thumbs-up"></span>';

        }else{
            $star_str .= '<span class="fa fa-thumbs-o-up"></span>';
        }
    }
    $star_str .= '</div>';
    return $star_str;

}
function get_active_stars( $star_value = 0  ) {


    $star_str = '<div class="starssss">';
    for($i=1; $i <= 5 ; $i++) { 
        if( $i <= $star_value ){
            $star_str .= '<span class="fa fa-thumbs-up"></span>';

        }else{
            $star_str .= '<span class="fa fa-thumbs-o-up"></span>';
        }
    }
    $star_str .= '</div>';
    echo $star_str;

}



function show_alert() {
    $CI = & get_instance();
    $msg = "";
    $alt_class = "";
    if ($CI->session->flashdata('success')) {
        $msg = $CI->session->flashdata('success');
        $alt_class = 'alert-success';
    } elseif ( $CI->session->flashdata('error')) {
        $msg = $CI->session->flashdata('error');
        $alt_class = 'alert-danger';
    } elseif (validation_errors()) {
        $alt_class = 'alert-danger';
        $msg = validation_errors();
    }
    if ($msg) {
        echo '<div class="alert ' . $alt_class . '">' . $msg . '</div>';
    }
}

function get_common_css_links( $page = ''  ){

  $links = '';

    //if( $page == 'home' ){
        $links .= '<link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">
            <!-- font Awesome CSS -->
            <link rel="stylesheet" href="'.base_url("assets/css/font-awesome.min.css").'">
            <!-- Main Styles CSS -->
            <link href="'.base_url("assets/css/style.css").'" rel="stylesheet">
            <link href="'.base_url("assets/css/responsive.css").'" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600" rel="stylesheet">';
        $links .= '<link href="'.base_url("assets/css/owl.carousel.min.css").'" rel="stylesheet">
            <link href="'.base_url("assets/css/owl.theme.default.min.css").'" rel="stylesheet">
            <link href="'.base_url("assets/css/bootstrap-select.min.css").'" rel="stylesheet">';
    //}
    //if( $page != 'home' ){
        //'<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">';
        $links .= '<link href="'.base_url("assets/css/datepicker.min.css").'" rel="stylesheet">
            <link href="'.base_url("assets/css/jquery.scrollbar.css").'" rel="stylesheet">
            <link href="'.base_url("assets/css/lightgallery.min.css").'" rel="stylesheet">';
    //}
    $links .= '<!--[if lt IE 9]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->';  

  echo $links;
} 

function get_common_js_links(){
  $links = '';
  $links = '<div id="google_translate_element" ></div> 
            <script type="text/javascript" src="'.base_url("assets/js/jquery.min.js").'" ></script>
            <script type="text/javascript" src="'.base_url("assets/js/bootstrap.min.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/owl.carousel.min.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/bootstrap-select.min.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/bootstrap-datepicker.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/rating.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/jquery.scrollbar.min.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/jquery.vticker-min.js").'"></script>
            <script type="text/javascript" src="'.base_url("assets/js/lightgallery.min.js").'"></script>
            <script src="'.base_url("assets/js/scripts.js").'"></script>

            <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
            <script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: "en", layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false},"google_translate_element");
              }
            </script>
            <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
            <script src="'.base_url("assets/js/custome.js").'"></script>
            ';
  echo $links;
}

function get_search_box(){
    
    $search_html = '<div class="input-group-btn place-box">
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate city-animate" style="display:none;" ></span>
                      <input type="text" class="form-control autocomplete_city" id="city_name" onblur="get_area_name();" name="city_name" >
                    </div>
                  <div class="input-group-btn city-box">
                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate area-animate" style="display:none;" ></span>
                    <input type="text" class="form-control autocomplete_area" id="area_name" onblur="fill_auto_input();" name="area_name"> 
                  </div>
                  <div class="icon-box">
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate company-animate" style="display:none;" ></span>
                      <input type="text" class="form-control autocomplete autocomplete-com" onblur="submit_search();" name="company_name" >
                  </div>
                  ';
    echo $search_html;
}

function search_bar_2(){
    $search_html = '<div class="search-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                        <div class="searchBar">
                            <form class="navbar-form navbar-search" role="search"  >
                                <div class="input-group">
                                    <div class="input-group-btn place-box">
                                        <div class="mapBox">
                                            <input class="form-control bs-autocomplete oneMap MapOn" id="city_name"
                                                   value="" placeholder="City..." type="text"
                                                   data-source="demo_source.php" data-hidden_field_id="city-code"
                                                   data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name" >
                                        </div>
                                    </div>
                                    <div class="input-group-btn city-box">
                                        <input class="form-control bs-autocomplete oneMap" id="area_name" value=""
                                               placeholder="Area..." type="text" data-source="demo_source.php"
                                               data-hidden_field_id="city-code" data-item_id="id"
                                               data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >

                                    </div>
                                    <div class="icon-box">
                                        <input class="form-control bs-autocomplete" id="client_cat_id" value=""
                                               placeholder="Resturant..." type="text"
                                               data-source="demo_source.php" data-hidden_field_id="city-code"
                                               data-item_id="id" data-item_label="cityName" autocomplete="off" name="company_name" >
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        echo $search_html;
}

function get_other_page_header( $remove_bg = '',$other_option = array() ){

    $ci =& get_instance();
    $header_html = '';
    $bg = 'header-bg';
    if( $remove_bg == 1 ){
      $bg = '';
    }

    $company_name = '';
    $get_city_name = '';
    $get_area_name = '';
    if( count( $other_option ) ){
        $company_name = $other_option['company_name'];
        $get_city_name = $other_option['get_city_name'];
        $get_area_name = $other_option['get_area_name'];
    }
    $user_data  = $ci->session->userdata( 'user_data' );
    $login_menu_mobile  = '<li><a href="#loginPop" data-toggle="modal" class="about-btn">login</a></li>';
    $login_menu  = '<li><a href="#loginPop" data-toggle="modal" class="logBtn">log in</a></li>';
    if( count($user_data) ){
         

        $login_menu  = '<li class="myprofileBtn">
                            <a href="#" class="about-btn myproBtn">my Profile</a>
                            <div class="myProfileBox">
                                <ul>
                                    <li><a href="'.base_url( 'users/profile' ).'">My Profile</a></li>
                                    <li><a href="#">My ratings</a></li>
                                    <li><a href="#">My reviews</a></li>
                                    <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                </ul>
                            </div>
                        </li>';
        $login_menu_mobile  = '<li class="myprofileBtn">
                                    <a href="#" class="about-btn myproBtn">my Profile</a>
                                    <div class="myProfileBox">
                                        <ul>
                                            <li><a href="'.base_url( 'users/profile' ).'">My Profile</a></li>
                                            <li><a href="#">My ratings</a></li>
                                            <li><a href="#">My reviews</a></li>
                                            <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                        </ul>
                                    </div>
                                </li>';
    }

    $header_html = '<div class="'.$bg.' PageHead navbar-fixed-top">
    <section class="main-menu1">
        <div class="container desktop-menu1">
            <div class="row">
                <div class="col-md-2 col-sm-3 logo-main">
                    <a href="'.base_url().'" class="logo-box1">
                      <img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-md-7 col-sm-5 menu-box">
                    <div class="searchBar">
                        <form class="navbar-form navbar-search" role="search"  >
                            <div class="input-group">
                                <div class="input-group-btn place-box">
                                    <div class="mapBox">
                                        <input class="form-control bs-autocomplete oneMap MapOn" id="city_name"
                                               value="'.$get_city_name.'" placeholder="City..." type="text"
                                               data-source="demo_source.php" data-hidden_field_id="city-code"
                                               data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name" >
                                    </div>
                                </div>
                                <div class="input-group-btn city-box">
                                    <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'"
                                           placeholder="Area..." type="text" data-source="demo_source.php"
                                           data-hidden_field_id="city-code" data-item_id="id"
                                           data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >

                                </div>
                                <div class="icon-box">
                                    <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'"
                                           placeholder="Resturant..." type="text"
                                           data-source="demo_source.php" data-hidden_field_id="city-code"
                                           data-item_id="id" data-item_label="cityName" autocomplete="off" name="company_name" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 menu-box">
                    <div class="right-menu1">
                        <ul>
                            <li>
                              <a href="#" onclick="show_business();" class="business-btn1">add business</a>
                            </li>
                            <li>
                              <a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a>
                            </li>
                                '.$login_menu.'
                            </li>
                            <li class="langBtn">
                              <a href="#" class="mylangBtn">
                                <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                              </a>
                              <div class="langDrop">
                                    <ul class="translation-icons" >
                                        <li>
                                            <a href="#" data-placement="0" data-langname="en" >English</a>
                                        </li>
                                        <li>
                                            <a href="#" data-placement="47" data-langname="kn" >Kananda</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                              <a href="#loginPop" data-toggle="modal" >
                                <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                              </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-main1">
            <div class="mobile-logo-box">
                <a href="#"><img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt=""></a>
            </div>
            <div class="mobile-menu">
                <ul>
                    <li><a href="#" onclick="show_business();" class="business-btn">add business</a></li>
                    <li><a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a></li>
                    <li>
                        '.$login_menu_mobile.'
                    </li>
                    <span class="user-login">   
                        <li>
                          <a href="#quickReply" data-toggle="modal" >
                            <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                          </a>
                        </li>
                        <li class="langBtn">
                              <a href="#" class="mylangBtn">
                            <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                          </a>
                          <div class="langDrop">
                                <ul class="translation-icons" >
                                    <li>
                                        <a href="" data-placement="0" data-langname="en" >English</a>
                                    </li>
                                    <li>
                                        <a href="#" data-placement="47" data-langname="kn" >Kananda</a>
                                    </li>
                                </ul>
                            </div>

                        </li>
                    </span>
                </ul>
            </div>
        </div>
        <div class="search-sec mobile-menu-main1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                        <div class="searchBar">
                            <form class="navbar-form navbar-search" role="search">
                                <div class="input-group">
                                    <div class="input-group-btn place-box">
                                        <div class="mapBox">
                                            <input class="form-control bs-autocomplete oneMap MapOn" id="city_name" value="'.$get_city_name.'" placeholder="City..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name"  >
                                        </div>
                                    </div>
                                    <div class="input-group-btn city-box">
                                        <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'" placeholder="City" type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >

                                    </div>
                                    <div class="icon-box">
                                        <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'" placeholder="Type the first two characters of a city name..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off"  name="company_name"  >

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><div class="menuDividerl"></div>';
  echo $header_html;
}

function get_sign_up_popup(){
  $header_html = '';
  $header_html = '<div class="quickReply">
                    <div class="modal fade" id="SignUpPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <span> Sign Up</span>
                                </div>
                                <div class="modal-body">
                                    <div class="reply-box">
                                        <div id="signup_msg"></div>
                                        <form name="signup_frm" id="signup_frm" action="" method="post" enctype="multipart/form-data" >
                                            <div class="form-group">
                                                <label for="names">Enter name<sup>*</sup></label>
                                                <input type="text" class="form-control" id="names" name="Names"  value="" >
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneNumber">Enter Phone Number</label>
                                                <div class="input-group PhoneDetail">
                                                    <div class="input-group-btn bs-dropdown-to-select-group">
                                                        <button type="button"
                                                                class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                                data-toggle="dropdown">
                                                            <span data-bind="bs-drp-sel-label">+91</span>
                                                            <input type="hidden" id="countrycode" name="Number_pre" data-bind="bs-drp-sel-value"
                                                                   value="">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" style="">
                                                            <li data-value="1"><a href="#">+91</a></li>
                                                            <li data-value="2"><a href="#">+92</a></li>
                                                            <li data-value="3"><a href="#">+93</a></li>
                                                            <li data-value="4"><a href="#">+94</a></li>
                                                        </ul>
                                                    </div>
                                                    <input type="text" class="form-control" name="signupphone" id="signupphone">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="emails">Enter E-mail</label>
                                                <input type="email" class="form-control" id="emails" name="Emails" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="pass">Create Password<sup>*</sup></label>
                                                <input type="password" class="form-control" id="pass" name="Passwords" >
                                            </div>
                                            <div class="form-group">
                                                <div class="btn-group">
                                                    <button class="replySubmit btn" type="submit">send details</button>
                                                    <button class="newUserBtn btn" type="button" data-target="#loginPop"
                                                            data-toggle="modal" data-dismiss="modal">existing user? log in
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="loginShare">
                                        <ul>
                                            <li><a href="#">sign up with google <span><i class="fa fa-google-plus"></i></span></a></li>
                                            <li><a href="#"><span><i class="fa fa-facebook-f"></i></span> sign up with facebook</a></li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="quick-adds">
                                    <div class="ad-tag">
                                        sponsered ads
                                    </div>
                                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>';
  echo $header_html;
}

function get_quick_reply_popup(){
  $popup_html = '';
  $popup_html = '<div class="quickReply">
    <div class="modal fade" id="quickReply" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span> quick reply</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box">
                        <p>Want to get the best Rates across Karnataka? Please submit your details.</p>

                        <form id="quickReplyForm">
                            <div class="form-group">
                                <label for="name">Enter your name</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Enter your E-mail</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label for="phoneNumber">Enter your Phone Number</label>

                                <div class="input-group PhoneDetail">
                                    <div class="input-group-btn bs-dropdown-to-select-group">
                                        <button type="button"
                                                class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                data-toggle="dropdown">
                                            <span data-bind="bs-drp-sel-label">+91</span>
                                            <input type="hidden" name="selected_value" data-bind="bs-drp-sel-value"
                                                   value="">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" style="">
                                            <li data-value="1"><a href="#">+91</a></li>
                                            <li data-value="2"><a href="#">+92</a></li>
                                            <li data-value="3"><a href="#">+93</a></li>
                                            <li data-value="4"><a href="#">+94</a></li>
                                        </ul>
                                    </div>
                                    <input type="text" class="form-control" name="phonenumber" id="phonenumber">

                                </div>
                            </div>
                            <div class="form-group otpDetail">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn ">
                                            get OTP
                                        </button>
                                    </div>
                                    <input type="text" value="" class="form-control" name="text"
                                           placeholder="Enter OTP">

                                </div>
                            </div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit">send details</button>
                            </div>
                            <div class="form-group">
                                <div class="emailsatusdisplay"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>';
  echo $popup_html;
}

function fill_drop_down( $data_arr = array(), $val_field ="" , $dis_field = "" , $selval = "" ){
    $str = '';
    foreach ( $data_arr as $key => $value) {
        $sel = "";
        if( $selval == $value[ $val_field ] ){
            $sel = "selected='selected'";
        }
        $str .= '<option value="'.$value[ $val_field ].'" '.$sel.' >'.$value[ $dis_field ].'</option>';
    }
    return $str;

}

function get_business_popup(){
    
    $popup_html = '';
    $popup_html = '<div class="quickReply">
    <div class="modal fade" id="businessPop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span> add business</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box">
                        <form name="add_business_frm" id="add_business_frm" action="" method="post" action="">
                            <div class="row">
                                <div id="add_business_msg"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nameU">Name<sup>*</sup></label>
                                        <input type="text" class="form-control" id="nameU" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="category">category<sup>*</sup> </label>
                                        <select class="selectpicker form-control" id="category" name="Category" required >
                                            <option>Select Category</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="localty">locality</label>
                                        <select class="selectpicker form-control" id="localty" name="localty" >
                                            <option>Select localty</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">address</label>
                                        <textarea id="address" rows="6" class="form-control" name="Address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="landMark">landmark</label>
                                        <input type="text" class="form-control" id="landMark" name="Landmark"  value="" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="namea">Name<sup>*</sup></label>
                                        <input type="text" class="form-control" id="namea" name="CompanyName" value="" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="phoneNumber">Enter Phone Number</label>
                                        <div class="input-group PhoneDetail">
                                            <div class="input-group-btn bs-dropdown-to-select-group">
                                                <button type="button"
                                                        class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                        data-toggle="dropdown">
                                                    <span data-bind="bs-drp-sel-label">+91</span>
                                                    <input type="hidden" name="Number_pre" data-bind="bs-drp-sel-value"
                                                           value="">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="">
                                                    <li data-value="1"><a href="#">+91</a></li>
                                                    <li data-value="2"><a href="#">+92</a></li>
                                                    <li data-value="3"><a href="#">+93</a></li>
                                                    <li data-value="4"><a href="#">+94</a></li>
                                                </ul>
                                            </div>
                                            <input type="text" value="" class="form-control" name="Number" id="phoneNumber" >

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="landLine">landline Number</label>
                                        <input type="text" class="form-control" id="landLine" name="Landline" value="">
                                    </div>
                                    <div class="mandotyFld">
                                        <div class="form-group">
                                            <label for="mandotory" class="mandotoryfld">* Mandatory Fields</label>
                                            <div class="checkbox">
                                                <input id="checkbox2" type="checkbox">
                                                <label for="checkbox2">
                                                    I am the owner of this Business
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn businessSubmit" id="add_business_submit" name="add_business_submit" >submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
  echo $popup_html ;
}



function get_thank_popup(){
    $thank_html ='' ;
    $thank_html ='<div class="quickReply">
                    <div class="modal fade" id="thankPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="reply-box loginBox">
                                        <p>Thanks for showing interest</p>
                                        <h4>We will get back to you soon.</h4>

                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>' ;
    echo $thank_html;

}

function get_forgot_password_popup(){
    
    $forgot_password_html ='' ;
    $forgot_password_html ='<div class="quickReply">
    <div class="modal fade" id="forgotPop" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span> Forgot Password</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box loginBox">
                        <p>Enter your registered email id.</p>

                        <p>We will send the user id along with a reset link to reset your password.</p>

                        <div id="forgot_msg"></div>
                        <form id="forgotpasswordform">
                            <div class="form-group">
                                <input type="email" class="form-control" id="forgotemail">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit">reset password
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>' ;
    echo $forgot_password_html ;
}


function get_new_password_popup(){
    
    $forgotuser_token_match = "";
    $forgotuser_token_valid = "";
    $CI = & get_instance();
    if(  strlen($CI->session->userdata("forgotuser_token_match")) > 0 ){
        $forgotuser_token_match = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Danger!</strong> '.$CI->session->userdata("forgotuser_token_match").'</div>';
    }
    
    if( strlen($CI->session->userdata("forgotuser_token_valid")) > 0 ){
        $forgotuser_token_valid = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Danger!</strong> '.$CI->session->userdata("forgotuser_token_valid").'</div>';
    }
    
    
    
    $forgot_new_password_html ='' ;
    $forgot_new_password_html ='<div class="quickReply">
    <div class="modal fade" id="newpasswordPop" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span> New Password</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box loginBox">                       
                        <div id="updateforgot_msg">
                        '.$forgotuser_token_match.'
                            '.$forgotuser_token_valid.'
                        </div>
                        <form id="newpasswordform">
                            <div class="form-group">
                                <label for="password">New Password</label>
                                <input type="password" class="form-control" id="newpassword">
                            </div>
                            <div class="form-group">
                                <label for="cpassword">Confirm Password</label>
                                <input type="password" class="form-control" id="confirmnewpassword">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit">Update password
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>' ;
    echo $forgot_new_password_html ;
}

/*
function get_thank_popup(){
    $thank_html ='' ;
    $thank_html ='<div class="quickReply">
                    <div class="modal fade" id="thankPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="reply-box loginBox">
                                        <p>Thanks for showing interest</p>
                                        <h4>We will get back to you soon.</h4>

                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>' ;
    echo $thank_html;

}

function get_forgot_password_popup(){
    
    $forgot_password_html ='' ;
    $forgot_password_html ='<div class="quickReply">
    <div class="modal fade" id="forgotPop" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span> Forgot Password</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box loginBox">
                        <p>Enter your registered email id.</p>

                        <p>We will send the user id along with a reset link to reset your password.</p>

                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" id="emailIds">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit" data-dismiss="modal"
                                        data-target="#thankPop" data-toggle="modal">reset password
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>' ;
    echo $forgot_password_html ;
}
*/
function get_login_popup(){
    $login_html ='' ;
    $login_html ='<div class="quickReply">
                        <div class="modal fade" id="loginPop" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <span> login</span>
                                    </div>
                                    <div class="modal-body">
                                        <div class="reply-box loginBox">

                                            <form name="login_frm" id="login_frm" method="post" > 

                                                <div class="form-group">
                                                    <label for="userId">user id</label>
                                                    <input type="text" class="form-control" id="email_id" name="email_id" required >
                                                </div>
                                                <div class="form-group">
                                                    <label for="psd">Password</label>
                                                    <input type="password" name="Password" id="password_login" class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <input id="checkbox1" type="checkbox">
                                                        <label for="checkbox1">
                                                            Remember me
                                                        </label>
                                                    </div>
                                                    <a href="#forgotPop" data-toggle="modal" data-dismiss="modal" class="forgot-psd">forgot password ?</a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div id="login_msg" class="" ></div>
                                                <div class="form-group">
                                                    <button class="replySubmit btn" type="submit" id="login_user" name="login_submit" >log in</button>
                                                    <br>
                                                    <a href="#SignUpPop" data-toggle="modal" data-dismiss="modal" class="newUserBtn">new
                                                        user? Sign up</a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="loginShare">
                                            <ul>
                                                <li><a href="#">log in with google <span><i class="fa fa-google-plus"></i></span></a></li>
                                                <li><a href="#"><span><i class="fa fa-facebook-f"></i></span> log in with facebook</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="quick-adds">
                                        <div class="ad-tag">
                                            sponsered ads
                                        </div>
                                        <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
  echo $login_html ;
}

function get_subscribe_popup(){

    $subscribe_html = '';
    $subscribe_html = '<div class="quickReply">
    <div class="modal fade" id="subscribeUs" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span>Subscribe Us</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box loginBox">
                        <p>Subscribe to get the latest & greatest Updates.</p>
                        <p>Enter your mail id below and join our Newsletter to get your pesonilized news updates</p>
                        <form name="subscribe_frm" id="subscribe_frm" action="" method="post" >
                            <div class="form-group">
                                <input type="email" class="form-control" id="s_email" name="Email">
                            </div>
                            <div class="clearfix"></div>
                            <div id="s_msg"></div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit" name="submit" id="submit" >Keep Me Posted
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>';
    echo $subscribe_html ;
}

function get_feedback_popup(){
    $feedback_html = '';
    $feedback_html = '<div class="quickReply">
                          <div class="modal fade" id="feedback" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                  <span> Submit Feedback</span> </div>
                                <div class="modal-body">
                                  <div class="reply-box loginBox feedback">
                                    <div class=" contact">
                                      <div id="fb_msg"></div>
                                      <form class="well" name="feedback_frm" id="feedback_frm" action="" method="post" >
                                        <div class="row">
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="name ">Enter Your Name*</label>
                                              <input type="text" class="form-control" name="Name" id="fb_name" value=""  >
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="email">Enter Your Mobile No*</label>
                                              <div class="input-group"> <span class="input-group-addon"><span>+91</span> </span>
                                                <input type="tel" class="form-control" id="fb_number" required="required" name="Number" value="" />
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="fb_email"> Enter Your E-Mail*</label>
                                              <input type="email" class="form-control" name="Email" id="fb_email" value="" >
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone"> 1) How Did you Hear about US?</label>
                                            </div>
                                            <div class="form-group form-radio">
                                              <label class="radio-inline" for="fb_social_media" >
                                                <input type="radio" name="HowHear" id="fb_social_media" value="SocialMedia" checked="true" >
                                                Social Media</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_advertising" value="Advertising" >
                                                Advertising </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_search_engine" value="SearchEngine" >
                                                Search Engine </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_friend_colleague" value="FriendColleague" >
                                                Friend/colleague </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_others" value="Others" >
                                                Others </label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="OtherTxt" id="fb_other_txt" value="" >
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone">2) What browser do u Use?</label>
                                            </div>
                                            <div class="form-group form-radio">
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_google_crome" value="GoogleCrome" checked="true" >
                                                Google crome </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_mozilla_firefox" value="MozillaFirefox" >
                                                Mozilla firefox </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_microsoft_edge" value="MicrosoftEdge" >
                                                Microsoft edge </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_safari" value="Safari" >
                                                safari </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_opera" value="Opera" >
                                                Opera </label>
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone">3) Which device do u use to access our content?</label>
                                            </div>
                                            <div class="form-group form-radio">
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_large_screen_device" value="LargeScreenDevice" checked="true" >
                                                Large screen Device </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_medium_screen_device" value="MediumScreenDevice" >
                                                Medium screen Device </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_small_screen_device" value="SmallScreenDevice" >
                                                Small screen Device </label>
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone">4) Are you satisfied to find us?</label>
                                            </div>
                                            <div class="form-group form-radio">
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_very_satisfied" value="VerySatisfied" checked="true" >
                                                Very satisfied </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_satisfied" value="Satisfied" >
                                                satisfied </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_neutral" value="Neutral" >
                                                Neutral </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_unsatisfied" value="Unsatisfied">
                                                unsatisfied </label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_very_unsatisfied" value="VeryUnsatisfied" >
                                                Very unsatisfied </label>
                                            </div>
                                            <div class="form-group">
                                                <textarea name="Notes" id="fb_notes"  class="form-control" ></textarea>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group btn-send">
                                              <button class="btn btn-send" type="submit" name="submit" value="submit" >Submit Feedback</button>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>';
    echo $feedback_html;
}

function get_feedback_thank_popup(){
    $feedback_thank_html ='' ;
    $feedback_thank_html ='<div class="quickReply">
                    <div class="modal fade" id="FeedbackThankPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="reply-box loginBox">
                                        <p>Thank you for your precious feedback.</p>
                                        <p>We will make sure to make our product better with time.</p>
                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    echo $feedback_thank_html;
}

function get_subscribe_thank_popup(){
    $thank_html ='' ;
    $thank_html ='<div class="quickReply">
                    <div class="modal fade" id="subscribeThankPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="reply-box loginBox">
                                        <p>Thank you for subscribing us.</p>
                                        <p>We promise we won\'t spam your inbox and will always give you the latest news.</p>
                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    echo $thank_html;
}

function get_current_city(){
  $ci =& get_instance();
  $current_city = "";
  $ip = $_SERVER['REMOTE_ADDR'];    
  $url = 'http://ip-api.com/json/'.$ip;
  $curl = curl_init($url);
  curl_setopt( $curl, CURLOPT_FAILONERROR, true);
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false);  
  $url_result = json_decode( curl_exec( $curl )  );
  $current_city = "";
  if( isset($url_result->city) && $url_result->city ){
    $current_city = $url_result->city;
  }
  return $current_city;

}
function get_footer_section(){

  $ci =& get_instance();
  $current_city = get_current_city();
  $slider_str = "";
  $trending_categories_data = $ci->general_model->get_all_rows(" * ",tbl( "category" )," AND Status='0' AND Parent='0' ORDER BY RAND() LIMIT 50" );
  if( isset( $trending_categories_data ) && count( $trending_categories_data )  ){
    $i = 1;
    $slider_str .= '<div class="col-md-12">
                      <div class="category-box">
                        <div class="owl-carousel owl-theme" id="category-box">';
    foreach( $trending_categories_data as $tc_key => $tc_value) {
        if( $i == 1  ){
            $slider_str .= '<div class="item">';
        }
        $slider_str .= '<div class="cat-box">
                          <h4><a href="'.base_url('cat/'.$tc_value['ID'] ).'">'.$tc_value['Name'].' in '.$current_city.'</a></h4>
                          <p>'.$tc_value['Description'].'</p>
                        </div>';
      if( $i%5 == 0  && $i != count( $trending_categories_data )    ){
        $slider_str .= '</div>
                      <div class="item">';
      }
      if( $i == count( $trending_categories_data )    ){
        $slider_str .= '</div>';
      }
      $i++;

    }

    $slider_str .= '</div>
                  </div>
                  <div class="clearfix"></div>
                  <p class="note-p">*Icons used in the website are from Flaticon.com</p>
              </div>';
  } 

  $footer_html = '<section class="widget-sec">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-6 col-xs-6 xs-full">
                                <div class="widget-box">
                                    <h4>about us</h4>
                                    <ul>
                                        <li><a href="'.base_url("pages/about-us").'">company overview</a></li>
                                        <li><a href="'.base_url("pages/policy").'">Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 xs-full">
                                <div class="widget-box">
                                    <h4>news & resources</h4>
                                    <ul>
                                        <li><a href="'.base_url("pages/news").'">in the news</a></li>
                                        <li><a href="'.base_url("pages/video").'">videos</a></li>
                                        <li><a href="#">social media</a></li>
                                        <li><a href="#">career at SMDC</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 xs-full">
                                <div class="widget-box">
                                    <h4>reach us</h4>
                                    <ul>
                                        <li><a href="#subscribeUs" data-toggle="modal" >subscribe us</a></li>
                                        <li><a href="#" onclick="show_business();" >add business</a></li>
                                        <li><a href="'.base_url("pages/contact-us").'">contact us</a></li>
                                        <li><a href="#feedback" data-toggle="modal" >submit feedback</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-6 xs-full">
                                <div class="paymentMmode">
                                    <h4>payment mode</h4>
                                    <ul>
                                        <li><a href="#"><img src="'.base_url("assets/img/visa.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/mastercard-1.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/mastercard.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/american-express.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/diners-club.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/discover.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/RuPay.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/PhonePe.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/COD.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/EMI-Option.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/Net-Banking.png").'" class="img-responsive" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            '.$slider_str.'
                        </div>
                    </div>
                </section>

                <section class="footer-sec">
                    <div class="dividerLine"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="footer-box">
                                    <ul>
                                        <li><a href="#">Careers</a></li>
                                        
                                        <li><a href="'.base_url("pages/policy").'">Privacy Policy</a></li>
                                        <li><a href="#">Copyright © 2017. Telcon Advertisement Private Limited. All rights reserved.</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12 ">
                                <div class="footer-social">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li>
                                        <a href="https://www.youtube.com/channel/UCspo2Dw3nhib2CaXk_yQ6NQ/featured" target="_blank">
                                            <i class="fa fa-facebook-f"></i>
                                        </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="scrollTop">
                    <a href="#" id="scrollTop">
                      <i class="fa fa-arrow-up" aria-hidden="true"></i>
                    </a>
                </div>';
                /*<li><a href="#">Disclaimer</a></li>*/
  echo $footer_html;
}


<?php

if ( !defined('BASEPATH')) exit('No direct script access allowed' );
//pre print function
function _P( $var ) {
    echo "<pre>";
    print_r( $var );
    echo "</pre>";
}

//table name tbl_ prefixed
function tbl( $var ){
    return "tbl_" . trim( $var );
}

function user_track( $category_id = ""  , $contract_id = "" ){
    $CI = & get_instance();
    $CI->load->model('general_model');
    $user_track  = $CI->session->userdata( 'user_track' );
        $ip = $_SERVER['REMOTE_ADDR'];
        $temp_url_result = @file_get_contents("http://ipinfo.io/{$ip}/json");
        $url_result = json_decode( $temp_url_result );
        
        $isExist = $CI->general_model->select_row( " ID,ComID,UserID " , tbl('visitor') , array('IP' => $ip , 'DATE(Created)' => date('Y-m-d')  ) );

        $user_data  = $CI->session->userdata( 'user_data' );
        $ins_data = array(
            'IP' => $ip, 
            'City' => (isset($url_result->city))?$url_result->city:"", 
            'Country' => ( isset($url_result->country) )?$url_result->country:"", 
            'Region' => ( isset($url_result->region) )?$url_result->region:"", 
            'ComID' => "", 
            'Status' => 0 , 
            'Created' => date('Y-m-d H:i:s'), 
        );
        if( isset($category_id) && $category_id ){
            $ins_data['CatID'] = $category_id;
        }
        $ins_data['UserID'] = ( isset($user_data['ID']) )?$user_data['ID']:0;
        $CI->general_model->insert_rows(  tbl('visitor') , $ins_data );
         
    return "";
}


function do_file_upload($upload_path = "", $file_name = "") {

    $CI = & get_instance();
    $config['upload_path'] = $upload_path; //$upload_path; //'./assets/upload/gift_product/'
    $config['allowed_types'] = 'gif|jpg|png';
    $ext = explode('.', $_FILES[$file_name]['name']);
    $new_name = time() . "." . $ext[1];
    $config['file_name'] = $new_name;
    $CI->load->library('upload', $config);
    if (!$CI->upload->do_upload( $file_name ) ){
        $result = "";
        //echo $CI->upload->display_errors($file_name);
    } else {
        $result = $new_name;
    }
    return $result;
}

function get_five_star( $star_value = 0, $c_id , $rating_id ){
$star_str = '<div class="new-starrr"  onclick="show_rating( &quot;'.$c_id.'&quot;,'.$rating_id.' );" id="c_stars_html_'.$c_id.'" >';
    for( $i=1; $i <= 5 ; $i++) { 
        if( $i <= $star_value ){
            $star_str .= '<span class="fa fa-star" ></span>';
        }else{
            if( ( $i-1 ) < $star_value ){
                $star_str .= '<span class="fa fa-star-half-o"></span>';    
            }else{
                $star_str .= '<span class="fa fa-star-o"></span>';    
            }
        }
    }
    $star_str .= '</div>';
    return $star_str;
}



function get_active_stars_r( $star_value = 0  ) {

    $star_str = '<div class="starssss">';
    for($i=1; $i <= 5 ; $i++) { 
        if( $i <= $star_value ){

            $star_str .= '<span class="fa fa-star"></span>';



        }else{

            $star_str .= '<span class="fa fa-star-o"></span>';

        }

    }

    $star_str .= '</div>';

    return $star_str;



}

function get_active_stars( $star_value = 0  ) {





    $star_str = '<div class="starssss">';

    for($i=1; $i <= 5 ; $i++) { 

        if( $i <= $star_value ){

            $star_str .= '<span class="fa fa-star"></span>';



        }else{

            $star_str .= '<span class="fa fa-star-o"></span>';

        }

    }

    $star_str .= '</div>';

    echo $star_str;



}







function show_alert() {

    $CI = & get_instance();

    $msg = "";

    $alt_class = "";

    if ($CI->session->flashdata('success')) {

        $msg = $CI->session->flashdata('success');

        $alt_class = 'alert-success';

    } elseif ( $CI->session->flashdata('error')) {

        $msg = $CI->session->flashdata('error');

        $alt_class = 'alert-danger';

    } elseif (validation_errors()) {
        $alt_class = 'alert-danger';
        $msg = validation_errors();
    }

    if ($msg) {
        
        echo '<div class="alert ' . $alt_class . '">' . $msg . '</div>';

    }

}



function get_common_css_links( $page = ''  ){



  $links = '';



    //if( $page == 'home' ){

        $links .= '<link rel="stylesheet" href="'.base_url("assets/css/bootstrap.min.css").'">

            <!-- font Awesome CSS -->

            <link rel="stylesheet" href="'.base_url("assets/css/font-awesome.min.css").'">

            <!-- Main Styles CSS -->

            <link href="'.base_url("assets/css/style.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/responsive.css").'" rel="stylesheet">

            <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600" rel="stylesheet">';

        $links .= '<link href="'.base_url("assets/css/owl.carousel.min.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/owl.theme.default.min.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/bootstrap-select.min.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/custom.css").'" rel="stylesheet">';


    //}

    //if( $page != 'home' ){

        //'<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">';

        $links .= '<link href="'.base_url("assets/css/datepicker.min.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/jquery.scrollbar.css").'" rel="stylesheet">

            <link href="'.base_url("assets/css/lightgallery.min.css").'" rel="stylesheet">';

    //}

    $links .= '<!--[if lt IE 9]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->';  



  echo $links;

} 



function get_common_js_links(){

  $links = '';

  $links = '<div id="google_translate_element" ></div> 

            <script type="text/javascript" src="'.base_url("assets/js/jquery.min.js").'" ></script>

            <script type="text/javascript" src="'.base_url("assets/js/bootstrap.min.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/owl.carousel.min.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/bootstrap-select.min.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/bootstrap-datepicker.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/rating.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/jquery.scrollbar.min.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/jquery.vticker-min.js").'"></script>

            <script type="text/javascript" src="'.base_url("assets/js/lightgallery.min.js").'"></script>

            <script src="'.base_url("assets/js/scripts.js").'"></script>



            <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

            <script type="text/javascript">

              function googleTranslateElementInit() {

                new google.translate.TranslateElement({pageLanguage: "en", layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false},"google_translate_element");

              }

            </script>

            <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

            <script src="'.base_url("assets/js/custome.js").'"></script>
    <!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"soDbq1kx0820/9", domain:"searchmaadi.com",dynamic: true};
(function() { var as = document.createElement("script"); as.type = "text/javascript";
 as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; 
 var s = document.getElementsByTagName("script")[0];
 s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=soDbq1kx0820/9" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->  
            ';

  echo $links;

}



function get_search_box(){

    

    $search_html = '<div class="input-group-btn place-box">
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate city-animate" style="display:none;" ></span>
                      <input type="text" class="form-control autocomplete_city" id="city_name" onblur="get_area_name();" name="city_name" >
                    </div>
                  <div class="input-group-btn city-box">
                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate area-animate" style="display:none;" ></span>
                    <input type="text" class="form-control autocomplete_area" id="area_name" onblur="fill_auto_input();" name="area_name">
                  </div>
                  <div class="icon-bo">
                      <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate company-animate" style="display:none;" ></span>
                      <input type="text" class="form-control autocomplete autocomplete-com" onblur="submit_search();" name="company_name" id="client_cat_id">
                  </div>';
    echo $search_html;

}



function search_bar_2(){

    $search_html = '<div class="search-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                        <div class="searchBar">
                            <form class="navbar-form navbar-search" role="search"  >
                                <div class="input-group">
                                    <div class="input-group-btn place-box">
                                        <div class="mapBox">
                                            <input class="form-control bs-autocomplete oneMap MapOn" id="city_name"
                                                   value="" placeholder="City..." type="text"
                                                   data-source="demo_source.php" data-hidden_field_id="city-code"
                                                   data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name" >
                                        </div>
                                    </div>

                                    <div class="input-group-btn city-box">
                                        <input class="form-control bs-autocomplete oneMap" id="area_name" value=""
                                               placeholder="Area..." type="text" data-source="demo_source.php"
                                               data-hidden_field_id="city-code" data-item_id="id"
                                               data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >
                                    </div>

                                    <div class="icon-bo">
                                        <input class="form-control bs-autocomplete" id="client_cat_id" value=""
                                               placeholder="Search Anything you Want" type="text"
                                               data-source="demo_source.php" data-hidden_field_id="city-code"
                                               data-item_id="id" data-item_label="cityName" autocomplete="off" name="company_name" >
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>';

        echo $search_html;

}



function get_other_page_header( $remove_bg = '',$other_option = array() ){
    
    $ci =& get_instance();
    $header_html = '';
    $bg = 'header-bg';
    if( $remove_bg == 1 ){
      $bg = '';
    }

    $company_name = '';
    $get_city_name = '';
    $get_area_name = '';
    if( count( $other_option ) ){

        //$company_name = substr($other_option['company_name'], 0, strrpos($other_option['company_name'], " "));
$company_name =$other_option['company_name'];
        $get_city_name = $other_option['get_city_name'];

        $get_area_name = $other_option['get_area_name'];

    }

    $user_data  = $ci->session->userdata( 'user_data' );

    $login_menu_mobile  = '<li><a href="#loginPop" data-toggle="modal" class="about-btn">login</a></li>';

    $login_menu  = '<li><a href="#loginPop" data-toggle="modal" class="logBtn">log in </a></li>
    <li>
                      <a href="#loginPop" data-toggle="modal" >
                        <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                      </a>
                    </li>';
    $user_menu = '<li>
                      <a href="#loginPop" data-toggle="modal" >
                        <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                      </a>
                    </li>';
                    
    $user_menu_mobile = '<li>
                          <a href="#quickReply" data-toggle="modal" >
                            <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                          </a>
                        </li>';

    if( count($user_data) ){
        
        $login_menu  = '<li class="myprofileBtn">
                            <a href="#" class="about-btn myproBtn">my Profile</a>
                            <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive proimage" alt="">
                            <div class="myProfileBox">
                                <ul>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">My Profile</a></li>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-ratings' ).'">My ratings</a></li>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-reviews' ).'">My reviews</a></li>
                                    <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                </ul>
                            </div>
                            
                        </li>';

        $login_menu_mobile  = '<li class="myprofileBtn">
                                    <a href="#" class="about-btn myproBtn">my Profile</a>
                                    <div class="myProfileBox">
                                        <ul>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">My Profile</a></li>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-ratings' ).'">My ratings</a></li>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-reviews' ).'">My reviews</a></li>
                                            <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                        </ul>
                                    </div>
                                </li>';

        // $user_menu = '<li >
        //                 <a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">
        //                     <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive proimagetab" alt="">
        //                 </a>
                        
        //             </li>';
                    /*<div class="myProfileBox">
                                <ul>
                                    <li><a href="'.base_url( 'users/profile' ).'">My Profile</a></li>
                                    <li><a href="#">My ratings</a></li>
                                    <li><a href="#">My reviews</a></li>
                                    <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                </ul>
                            </div>*/

    $user_menu_mobile = '<li class="myprofileBtn">
                            <a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">
                                <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                            </a>
                            
                        </li>';

    }



    $header_html = '<div class="'.$bg.' PageHead navbar-fixed-top">
    <section class="main-menu1">
        <div class="container desktop-menu1">
            <div class="row">
                <div class="col-md-2 col-sm-3 logo-main">
                    <a href="'.base_url().'" class="logo-box1">
                      <img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="col-md-6 col-sm-5 menu-box">
                    <div class="searchBar">
                        <form class="navbar-form navbar-search" role="search"  >
                            <div class="input-group">
                                <div class="input-group-btn place-box">
                                    <div class="mapBox">
                                        <input class="form-control bs-autocomplete oneMap MapOn" id="city_name"
                                               value="'.$get_city_name.'" placeholder="City..." type="text"
                                               data-source="demo_source.php" data-hidden_field_id="city-code"
                                               data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name" >
                                    </div>
                                </div>

                                <div class="input-group-btn city-box">
                                    <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'"
                                           placeholder="Area..." type="text" data-source="demo_source.php"
                                           data-hidden_field_id="city-code" data-item_id="id"
                                           data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >
                                </div>

                                <div class="icon-bo">
                                    <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'"
                                           placeholder="Search Anything you Want" type="text"
                                           data-source="demo_source.php" data-hidden_field_id="city-code"
                                           data-item_id="id" data-item_label="cityName" autocomplete="off" name="company_name" >
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 menu-box">
                    <div class="right-menu1">
                        <ul>
                            <li>
                              <a href="#" onclick="show_business();" class="business-btn1">add business</a>
                            </li>
                            <li>
                              <a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a>
                            </li>
                            '.$login_menu.'
                            
                            <li class="langBtn">
                              <a href="#" class="mylangBtn">
                                <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                              </a>
                              <div class="langDrop">
                                    <ul class="translation-icons" >
                                        <li>
                                            <a href="#" data-placement="0" data-langname="en" >English</a>
                                        </li>
                                        <li>
                                            <a href="#" data-placement="47" data-langname="kn" >Kannada</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            
                            
                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <div class="mobile-menu-main1">

            <div class="mobile-logo-box">

                <a href="'.base_url().'"><img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt=""></a>

            </div>

            <div class="mobile-menu">

                <ul>

                    <li><a href="#" onclick="show_business();" class="business-btn">add business</a></li>
                    <li><a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a></li>
                    '.$login_menu_mobile.'
                    <span class="user-login"> 
                        '.$user_menu_mobile.'
                        <li class="langBtn">
                            <a href="#" class="mylangBtn">
                                <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                            </a>
                            <div class="langDrop">
                                <ul class="translation-icons" >
                                    <li>
                                        <a href="" data-placement="0" data-langname="en" >English</a>
                                    </li>
                                    <li>
                                        <a href="#" data-placement="47" data-langname="kn" >Kannada</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                    </span>

                </ul>

            </div>

        </div>

        <div class="search-sec mobile-menu-main1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                        <div class="searchBar">
                            <form class="navbar-form navbar-search" role="search">
                                <div class="input-group">
                                    <div class="input-group-btn place-box">
                                        <div class="mapBox">
                                            <input class="form-control bs-autocomplete oneMap MapOn" id="city_name" value="'.$get_city_name.'" placeholder="City..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name"  >

                                        </div>

                                    </div>

                                    <div class="input-group-btn city-box">

                                        <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'" placeholder="City" type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >



                                    </div>

                                    <div class="icon-box">

                                        <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'" placeholder="Type the first two characters of a city name..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off"  name="company_name"  >



                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

</div><div class="menuDividerl"></div>';

  echo $header_html;

}


function get_other_page_header_after_login( $remove_bg = '',$other_option = array() ){
    
    $ci =& get_instance();
    $header_html = '';
    $bg = 'header-bg';
    if( $remove_bg == 1 ){
      $bg = '';
    }

    $company_name = '';
    $get_city_name = '';
    $get_area_name = '';
    if( count( $other_option ) ){
        $company_name = $other_option['company_name'];
        $get_city_name = $other_option['get_city_name'];
        $get_area_name = $other_option['get_area_name'];
    }

    $user_data  = $ci->session->userdata( 'user_data' );
    $login_menu_mobile  = '<li><a href="#loginPop" data-toggle="modal" class="about-btn">login</a></li>';
    $login_menu  = '<li><a href="#loginPop" data-toggle="modal" class="logBtn">log in</a></li>';
    $user_menu = '<li>
                      <a href="#loginPop" data-toggle="modal" >
                        <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                      </a>
                    </li>';
    $user_menu_mobile = '<li>
                          <a href="#quickReply" data-toggle="modal" >
                            <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                          </a>
                        </li>';
    if( count( $user_data ) ){
        
        $login_menu  = '<li class="myprofileBtn ">
                            <a href="#" class="about-btn myproBtn">my Profile</a>
                            <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive proimage" alt="">
                            <div class="myProfileBox">
                                <ul>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">My Profile</a></li>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-ratings' ).'">My ratings</a></li>
                                    <li><a href="'.base_url( 'users/profile/?active_tab=my-reviews' ).'">My reviews</a></li>
                                    <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                </ul>
                            </div>
                        </li>';

        $login_menu_mobile  = '<li class="myprofileBtn">
                                    <a href="#" class="about-btn myproBtn">my Profile</a>
                                    <div class="myProfileBox">
                                        <ul>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">My Profile</a></li>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-ratings' ).'">My ratings</a></li>
                                            <li><a href="'.base_url( 'users/profile/?active_tab=my-reviews' ).'">My reviews</a></li>
                                            <li><a href="'.base_url( 'index/do_logout' ).'">log out</a></li>
                                        </ul>
                                    </div>
                                </li>';

        // $user_menu = '<li >
        //                 <a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">
        //                     <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
        //                 </a>
                        
        //             </li>';

    $user_menu_mobile = '<li class="myprofileBtn">
                            <a href="'.base_url( 'users/profile/?active_tab=my-profile' ).'">
                                <img src="'.base_url('assets/img/user-icon.png').'" class="img-responsive" alt="">
                            </a>
                            
                        </li>';

    }



    $header_html = '<div class="aboutPage-body">
    <div class="header-bg PageHead navbar-fixed-top">
    <section class="main-menu1">
        <div class="container-fluid desktop-menu1">
            <div class="row">
                <div class="col-md-2 col-sm-3 logo-main">
                    <a href="'.base_url().'" class="logo-box1">
                      <img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt="searchmaadi.com">
                    </a>
                </div>
                <div class="col-md-6 col-sm-5 menu-box">
                    <div class="searchBar">
                        <form class="navbar-form navbar-search" role="search">
                            <div class="input-group">
                                <div class="input-group-btn place-box">
                                    <div class="mapBox">
                                        <input class="form-control bs-autocomplete oneMap MapOn" id="city_name"
                                               value="'.$get_city_name.'" placeholder="City..." type="text"
                                               data-source="demo_source.php" data-hidden_field_id="city-code"
                                               data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name" >
                                    </div>
                                </div>
                                <div class="input-group-btn city-box">
                                    <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'"
                                           placeholder="Area..." type="text" data-source="demo_source.php"
                                           data-hidden_field_id="city-code" data-item_id="id"
                                           data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >

                                </div>
                                <div class="icon-box">
                                    <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'"
                                           placeholder="Search Anything you Want" type="text"
                                           data-source="demo_source.php" data-hidden_field_id="city-code"
                                           data-item_id="id" data-item_label="cityName" autocomplete="off" name="company_name" >

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 menu-box">
                    <div class="right-menu1">
                        <ul>

                            <li>
                                <a href="#" onclick="show_business();" class="business-btn1">add business</a>
                            </li>
                            <li>
                                <a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a>
                            </li>
                            '.$login_menu.'
                            
                            <li class="langBtn">
                              <a href="#" class="mylangBtn">
                                <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                              </a>
                              <div class="langDrop">
                                    <ul class="translation-icons" >
                                        <li>
                                            <a href="#" data-placement="0" data-langname="en" >English</a>
                                        </li>
                                        <li>
                                            <a href="#" data-placement="47" data-langname="kn" >Kannada</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> 
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-main1">
            <div class="mobile-logo-box">
                <a href="'.base_url().'"><img src="'.base_url('assets/img/logo2.png').'" class="img-responsive" alt=""></a>
            </div>
            <div class="mobile-menu">
                <ul>
                    <li><a href="#" onclick="show_business();" class="business-btn">add business</a></li>
                    <li><a href="'.base_url( 'pages/about-us' ).'" class="about-btn">about us</a></li>
                    '.$login_menu_mobile.'
                    <span class="user-login"> 
                        '.$user_menu_mobile.'
                        <li class="langBtn">
                            <a href="#" class="mylangBtn">
                                <img src="'.base_url('assets/img/change-acc.png').'" class="img-responsive" alt="">
                            </a>
                            <div class="langDrop">
                                <ul class="translation-icons" >
                                    <li>
                                        <a href="" data-placement="0" data-langname="en" >English</a>
                                    </li>
                                    <li>
                                        <a href="#" data-placement="47" data-langname="kn" >Kannada</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                    </span>
                </ul>
            </div>
        </div>
        <div class="search-sec mobile-menu-main1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 ">
                        <div class="searchBar">
                            <form class="navbar-form navbar-search" role="search">
                                <div class="input-group">
                                    <div class="input-group-btn place-box">
                                        <div class="mapBox">
                                            <input class="form-control bs-autocomplete oneMap MapOn" id="city_name" value="'.$get_city_name.'" placeholder="City..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="get_area_name();" name="city_name"  >
                                        </div>
                                    </div>
                                    <div class="input-group-btn city-box">
                                        <input class="form-control bs-autocomplete oneMap" id="area_name" value="'.$get_area_name.'" placeholder="City" type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" onblur="fill_auto_input();" name="area_name" >

                                    </div>
                                    <div class="icon-box">
                                        <input class="form-control bs-autocomplete" id="client_cat_id" value="'.$company_name.'" placeholder="Type the first two characters of a city name..." type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off"  name="company_name"  >
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="menuDividerl"></div>';

  echo $header_html;

}

function get_sign_up_popup(){
    $CI = & get_instance();
    $CI->load->library('google');
    $googleLogInAUrl = $CI->google->loginURL();
    include('fb_lib/src/Facebook/autoload.php');
    $fb = new Facebook\Facebook([
      'app_id' => '969077476573226', // Replace {app-id} with your app id
      'app_secret' => '64cd57a047c8003a376aca84e9fe69fc',
      'default_graph_version' => 'v2.2',
    ]);
    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['email']; // Optional permissions
    $loginUrl = $helper->getLoginUrl(base_url( 'index/fb_login' ), $permissions);

  $header_html = '';

  $header_html = '<div class="quickReply">
                    <div class="modal fade" id="SignUpPop" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <span class="bg-red"> Sign Up</span>
                                </div>

                                <div class="modal-body">
                                    <div class="reply-box">
                                        <div id="signup_msg"></div>
                                        <form name="signup_frm" id="signup_frm" action="" method="post" enctype="multipart/form-data" >
                                            <div class="form-group">
                                                <label for="names">Enter name<sup>*</sup></label>
                                                <input type="text" class="form-control" id="names" name="Names"  value="" required >
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneNumber">Enter Phone Number</label>
                                                <div class="input-group PhoneDetail">
                                                    <div class="input-group-btn bs-dropdown-to-select-group">
                                                        <button type="button"
                                                                class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                                data-toggle="dropdown">
                                                            <span data-bind="bs-drp-sel-label">+91</span>
                                                            <input type="hidden" id="countrycode" name="Number_pre" data-bind="bs-drp-sel-value"
                                                                   value="">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" style="">
                                                            <li data-value="1"><a href="#">+91</a></li>
                                                            <li data-value="2"><a href="#">+92</a></li>
                                                            <li data-value="3"><a href="#">+93</a></li>
                                                            <li data-value="4"><a href="#">+94</a></li>
                                                        </ul>
                                                    </div>
                                                    <input type="text" class="form-control" name="signupphone" id="signupphone">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="emails">Enter E-mail<sup>*</sup></label>

                                                <input type="email" class="form-control" id="emails" name="Emails" value="" required>

                                            </div>

                                            <div class="form-group">
                                                <label for="pass">Create Password<sup>*</sup></label>
                                                <input type="password" class="form-control" id="pass" name="Passwords" required>
                                            </div>

                                            <div class="form-group">
                                                <div class="btn-group">
                                                    <button class="replySubmit btn" type="submit">Sign Up</button>
                                                    <button class="newUserBtn btn" type="button" data-target="#loginPop"
                                                            data-toggle="modal" data-dismiss="modal">existing user? log in
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="loginShare">

                                        <ul>

                                            <li><a href="'.$googleLogInAUrl.'">sign up with google <span><i class="fa fa-google-plus"></i></span></a></li>

                                            <li><a href="'.$loginUrl.'"><span><i class="fa fa-facebook-f"></i></span> sign up with facebook</a></li>

                                        </ul>



                                    </div>

                                </div>

                                <div class="quick-adds">

                                    <div class="ad-tag">

                                        sponsered ads

                                    </div>

                                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">

                                </div>

                            </div>



                        </div>

                    </div>

                </div>';

  echo $header_html;

}



function get_quick_reply_popup(){

  $popup_html = '';
  $popup_html= '
<div class="quickReply">
    <div class="modal fade" id="quickReply" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span id="newex"> quick reply</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box">
                        <p>Please submit your details, we\'ll get back to you</p>
                        <div id="qr_msg" ></div>
                        <form name="quick_reply_detail" id="quick_reply_detail" action="#" method="post" >
                            <div class="form-group">
                                <label for="name">Enter your name<sub>*</sub></label>
                                <input type="text" class="form-control" id="qr_name" name="qr_name" required="" >
                            </div>
                            <div class="form-group">
                                <label for="email">Enter your E-mail<sub>*</sub></label>
                                <input type="email" class="form-control" id="qr_email" name="qr_email"  required="" >
                            </div>
                            <div class="form-group">
                                <label for="phoneNumber">Enter your Phone Number<sub>*</sub></label>
                                <div class="input-group PhoneDetail">
                                    <div class="input-group-btn bs-dropdown-to-select-group">
                                        <button type="button"
                                                class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                data-toggle="dropdown">
                                            <span data-bind="bs-drp-sel-label">+91</span>
                                            <input type="hidden" data-bind="bs-drp-sel-value" value="" id="qr_number_pre" name="qr_number_pre"  required="" >
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" style="">
                                            <li data-value="1"><a href="#">+91</a></li>
                                            <li data-value="2"><a href="#">+92</a></li>
                                            <li data-value="3"><a href="#">+93</a></li>
                                            <li data-value="4"><a href="#">+94</a></li>
                                        </ul>
                                    </div>
                                    <input type="text" value="" class="form-control" id="qr_number" name="qr_number"  required=""  >
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="replySubmit btn" type="submit" name="send_details">send details</button>
                                <!-- <button class="replySubmit btn" type="submit" data-dismiss="modal"
                                        data-target="#thankPop" data-toggle="modal">send details
                                </button> -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="quick-adds">
                    <div class="ad-tag">
                        sponsered ads
                    </div>
                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </div>
</div>';
  /*$popup_html = '<div class="quickReply">
    <div class="modal fade" id="quickReply" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="bg-red"> quick reply</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box">
                        <p>Want to get the best Rates across Karnataka? Please submit your details.</p>
                        <form id="quickReplyForm">
                            <div class="form-group">
                                <label for="name">Enter your name</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Enter your E-mail</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label for="phoneNumber">Enter your Phone Number</label>
                                <div class="input-group PhoneDetail">
                                    <div class="input-group-btn bs-dropdown-to-select-group">
                                        <button type="button"
                                                class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                data-toggle="dropdown">
                                            <span data-bind="bs-drp-sel-label">+91</span>
                                            <input type="hidden" name="selected_value" data-bind="bs-drp-sel-value"
                                                   value="">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" style="">
                                            <li data-value="1"><a href="#">+91</a></li>
                                            <li data-value="2"><a href="#">+92</a></li>
                                            <li data-value="3"><a href="#">+93</a></li>
                                            <li data-value="4"><a href="#">+94</a></li>
                                        </ul>
                                    </div>
                                    <input type="text" class="form-control" name="phonenumber" id="phonenumber">
                                </div>
                            </div>
                            <div class="form-group otpDetail">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn ">
                                            get OTP
                                        </button>

                                    </div>

                                    <input type="text" value="" class="form-control" name="text"

                                           placeholder="Enter OTP">



                                </div>

                            </div>

                            <div class="form-group">

                                <button class="replySubmit btn" type="submit">send details</button>

                            </div>

                            <div class="form-group">

                                <div class="emailsatusdisplay"></div>

                            </div>

                        </form>

                    </div>

                </div>

                <div class="quick-adds">

                    <div class="ad-tag">

                        sponsered ads

                    </div>

                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">

                </div>
            </div>
        </div>
    </div>
</div>';*/
  echo $popup_html;
}



function fill_drop_down( $data_arr = array(), $val_field ="" , $dis_field = "" , $selval = "" ){

    $str = '';

    foreach ( $data_arr as $key => $value) {

        $sel = "";

        if( $selval == $value[ $val_field ] ){

            $sel = "selected='selected'";

        }

        $str .= '<option value="'.$value[ $val_field ].'" '.$sel.' >'.$value[ $dis_field ].'</option>';

    }

    return $str;
}



function get_business_popup(){
    /*<select class="selectpicker form-control" id="category" name="Category" required >
        <option>Select Category</option>
    </select>
    <select class="selectpicker form-control" id="localty" name="localty" >
        <option>Select localty</option>
    </select>
    <input type="text" class="form-control bs-autocomplete oneMap MapOn ui-autocomplete-input" id="category" name="Category" required data-source="" data-hidden_field_id="category-code"
                                                   data-item_id="id" data-item_label="categoryName" autocomplete="off" >
    */
    $popup_html = '';
    $popup_html = '<div class="quickReply">
    <div class="modal fade" id="businessPop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span d="newex" class="bg-red" > add business</span>
                </div>
                <div class="modal-body">
                    <div class="reply-box">
                        <form name="add_business_frm" id="add_business_frm" action="" method="post" action="">
                            <div class="row">
                                <div id="add_business_msg"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nameU">Name<sup>*</sup></label>
                                        <input type="text" class="form-control" id="ab_name" name="ab_name" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="category">category<sup>*</sup> </label>
                                        <input class="form-control bs-autocomplete ui-autocomplete-input" id="ab_category" value="" placeholder="" type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" autocomplete="off" name="ab_category" required >
                                    </div>

                                    <div class="form-group">
                                        <label for="localty">locality</label>
                                        <input class="form-control bs-autocomplete ui-autocomplete-input" id="ab_localty" value="" placeholder="" type="text" data-source="demo_source.php" data-hidden_field_id="city-code" data-item_id="id" data-item_label="cityName" data-std_code="std_code" autocomplete="off" name="ab_localty" required >
                                    </div>

                                    <div class="form-group">
                                        <label for="address">address</label>
                                        <textarea id="ab_address" rows="6" class="form-control" name="ab_address" id="ab_address" ></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="landMark">landmark</label>
                                        <input type="text" class="form-control" name="ab_landmark" id="ab_landmark"  value="" >
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="namea">Company Name<sup>*</sup></label>
                                        <input type="text" class="form-control" name="ab_companyname" id="ab_companyname" value="" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="phoneNumber">Enter Phone Number<sup>*</sup></label>
                                        <div class="input-group PhoneDetail">
                                            <div class="input-group-btn bs-dropdown-to-select-group">
                                                <button type="button"
                                                        class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select"
                                                        data-toggle="dropdown">
                                                    <span data-bind="bs-drp-sel-label">+91</span>
                                                    <input type="hidden" id="ab_number_pre" name="ab_number_pre" data-bind="bs-drp-sel-value"
                                                           value="">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="">
                                                    <li data-value="1"><a href="#">+91</a></li>
                                                    <li data-value="2"><a href="#">+92</a></li>
                                                    <li data-value="3"><a href="#">+93</a></li>
                                                    <li data-value="4"><a href="#">+94</a></li>
                                                </ul>
                                            </div>
                                            <input type="text" value="" class="form-control" name="ab_number" id="ab_number" required >
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="landLine">landline Number</label>
                                        <div class="input-group PhoneDetail">
                                        <div class="input-group-btn input-group-area-code">
                                            <input type="text" class="form-control" id="ab_area_code" name="ab_area_code" value="" placeholder="Area Code" >
                                        </div> 
                                        <input type="text" class="form-control" id="ab_landline" name="ab_landline" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ab_email">E-mail Id</label>
                                        <input type="text" class="form-control" id="ab_email" name="ab_email" value="">
                                    </div>

                                    <div class="mandotyFld">

                                        <div class="form-group">
                                            <label for="mandotory" class="mandotoryfld">* Mandatory Fields</label>
                                            <div class="checkbox">
                                                <input id="checkbox2" type="checkbox" id="ab_owner" name="ab_owner" required checked="checked" >
                                                <label for="checkbox2">
                                                    I am the owner of this Business
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn businessSubmit" id="add_business_submit" name="add_business_submit" >submit</button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';

  echo $popup_html ;

}







function get_thank_popup(){

    $thank_html ='' ;

    $thank_html ='<div class="quickReply">

                    <div class="modal fade" id="thankPop" role="dialog">

                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-body">

                                    <div class="reply-box loginBox">

                                        <p>Thanks for showing interest</p>

                                        <h4>We will get back to you soon.</h4>



                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>' ;

    echo $thank_html;



}



function get_forgot_password_popup(){

    

    $forgot_password_html ='' ;

    $forgot_password_html ='<div class="quickReply">

    <div class="modal fade" id="forgotPop" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="bg-red"> Forgot Password</span>
                </div>

                <div class="modal-body">
                    <div class="reply-box loginBox">
                        <p>Enter your registered email id.</p>
                        <p>We will send the user id along with a reset link to reset your password.</p>


                        <div id="forgot_msg"></div>

                        <form id="forgotpasswordform">

                            <div class="form-group">

                                <input type="email" class="form-control" id="forgotemail">

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">

                                <button class="replySubmit btn" type="submit">reset password

                                </button>

                            </div>

                        </form>

                    </div>



                </div>

                <div class="clearfix"></div>

                <div class="quick-adds">

                    <div class="ad-tag">

                        sponsered ads

                    </div>

                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">

                </div>

            </div>



        </div>

    </div>

</div>' ;

    echo $forgot_password_html ;

}





function get_new_password_popup(){

    

    $forgotuser_token_match = "";

    $forgotuser_token_valid = "";

    $CI = & get_instance();

    if(  strlen($CI->session->userdata("forgotuser_token_match")) > 0 ){

        $forgotuser_token_match = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Danger!</strong> '.$CI->session->userdata("forgotuser_token_match").'</div>';

    }

    

    if( strlen($CI->session->userdata("forgotuser_token_valid")) > 0 ){

        $forgotuser_token_valid = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Danger!</strong> '.$CI->session->userdata("forgotuser_token_valid").'</div>';

    }

    

    

    

    $forgot_new_password_html ='' ;

    $forgot_new_password_html ='<div class="quickReply">

    <div class="modal fade" id="newpasswordPop" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <span> New Password</span>

                </div>

                <div class="modal-body">

                    <div class="reply-box loginBox">                       

                        <div id="updateforgot_msg">

                        '.$forgotuser_token_match.'

                            '.$forgotuser_token_valid.'

                        </div>

                        <form id="newpasswordform">

                            <div class="form-group">

                                <label for="password">New Password</label>

                                <input type="password" class="form-control" id="newpassword">

                            </div>

                            <div class="form-group">

                                <label for="cpassword">Confirm Password</label>

                                <input type="password" class="form-control" id="confirmnewpassword">

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">

                                <button class="replySubmit btn" type="submit">Update password

                                </button>

                            </div>

                        </form>

                    </div>



                </div>

                <div class="clearfix"></div>

                <div class="quick-adds">

                    <div class="ad-tag">

                        sponsered ads

                    </div>

                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">

                </div>

            </div>



        </div>

    </div>

</div>' ;

    echo $forgot_new_password_html ;

}



/*

function get_thank_popup(){

    $thank_html ='' ;

    $thank_html ='<div class="quickReply">

                    <div class="modal fade" id="thankPop" role="dialog">

                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-body">

                                    <div class="reply-box loginBox">

                                        <p>Thanks for showing interest</p>

                                        <h4>We will get back to you soon.</h4>



                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>' ;

    echo $thank_html;



}



function get_forgot_password_popup(){

    

    $forgot_password_html ='' ;

    $forgot_password_html ='<div class="quickReply">

    <div class="modal fade" id="forgotPop" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <span> Forgot Password</span>

                </div>

                <div class="modal-body">

                    <div class="reply-box loginBox">

                        <p>Enter your registered email id.</p>



                        <p>We will send the user id along with a reset link to reset your password.</p>



                        <form>

                            <div class="form-group">

                                <input type="email" class="form-control" id="emailIds">

                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">

                                <button class="replySubmit btn" type="submit" data-dismiss="modal"

                                        data-target="#thankPop" data-toggle="modal">reset password

                                </button>

                            </div>

                        </form>

                    </div>



                </div>

                <div class="clearfix"></div>

                <div class="quick-adds">

                    <div class="ad-tag">

                        sponsered ads

                    </div>

                    <img src="'.base_url("assets/img/placeholder-image.png" ).'" class="img-responsive" alt="">

                </div>

            </div>



        </div>

    </div>

</div>' ;

    echo $forgot_password_html ;

}

*/

function get_login_popup(  ){

    $CI = & get_instance();
    $CI->load->library('google');
    $googleLogInAUrl = $CI->google->loginURL();

    include('fb_lib/src/Facebook/autoload.php');
    $fb = new Facebook\Facebook([
      'app_id' => '969077476573226', // Replace {app-id} with your app id
      'app_secret' => '64cd57a047c8003a376aca84e9fe69fc',
      'default_graph_version' => 'v2.2',
    ]);
    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['email']; // Optional permissions
    $loginUrl = $helper->getLoginUrl(base_url( 'index/fb_login' ), $permissions);
    $login_html ='';
    $login_html ='<div class="quickReply">
                        <div class="modal fade" id="loginPop" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        <span id="newex"> login</span>

                                    </div>

                                    <div class="modal-body">

                                        <div class="reply-box loginBox">



                                            <form name="login_frm" id="login_frm" method="post" > 



                                                <div class="form-group">

                                                    <label for="userId">Email id</label>

                                                    <input type="text" class="form-control" id="email_id" name="email_id" required >

                                                </div>

                                                <div class="form-group">

                                                    <label for="psd">Password</label>

                                                    <input type="password" name="Password" id="password_login" class="form-control" required>

                                                </div>

                                                <div class="form-group">

                                                    <div class="checkbox">

                                                        <input id="checkbox1" type="checkbox">

                                                        <label for="checkbox1">

                                                            Remember me

                                                        </label>

                                                    </div>

                                                    <a href="#forgotPop" data-toggle="modal" data-dismiss="modal" class="forgot-psd">forgot password ?</a>

                                                </div>

                                                <div class="clearfix"></div>

                                                <div id="login_msg" class="" ></div>

                                                <div class="form-group">

                                                    <button class="replySubmit btn" type="submit" id="login_user" name="login_submit" >log in</button>

                                                    <br>

                                                    <a href="#SignUpPop" data-toggle="modal" data-dismiss="modal" class="newUserBtn">new

                                                        user? Sign up</a>

                                                </div>

                                            </form>

                                        </div>

                                        <div class="loginShare">
                                            <ul>
                                                <li><a href="'.$googleLogInAUrl.'">log in with google<span><i class="fa fa-google-plus"></i></span></a></li>
                                                <li><a href="'.htmlspecialchars( $loginUrl ).'"><span><i class="fa fa-facebook-f"></i></span> log in with facebook</a></li>
                                            </ul>

                                        </div>

                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="quick-adds">

                                        <div class="ad-tag">

                                            sponsered ads

                                        </div>

                                        <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>';

  echo $login_html ;

}



function get_subscribe_popup(){



    $subscribe_html = '';

    $subscribe_html = '<div class="quickReply">

    <div class="modal fade" id="subscribeUs" role="dialog">

        <div class="modal-dialog">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <span>Subscribe Us</span>

                </div>

                <div class="modal-body">

                    <div class="reply-box loginBox">

                        <p>Subscribe to get the latest & greatest Updates.</p>

                        <p>Enter your mail id below and join our Newsletter to get your personalized news updates</p>

                        <form name="subscribe_frm" id="subscribe_frm" action="" method="post" >

                            <div class="form-group">

                                <input type="email" class="form-control" id="s_email" name="Email">

                            </div>

                            <div class="clearfix"></div>

                            <div id="s_msg"></div>

                            <div class="form-group">

                                <button class="replySubmit btn" type="submit" name="submit" id="submit" >Keep Me Posted

                                </button>

                            </div>

                        </form>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="quick-adds">

                    <div class="ad-tag">

                        sponsered ads

                    </div>

                    <img src="'.base_url('assets/img/placeholder-image.png').'" class="img-responsive" alt="">

                </div>

            </div>



        </div>

    </div>

</div>';

    echo $subscribe_html ;

}


 
function get_feedback_popup(){

    $feedback_html = '';
    $feedback_submit = base_url('pages/feedback_submit');
    $feedback_html = '<div class="quickReply">
                            <div class="modal fade" id="feedback" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <span class="bg-red"> Submit Feedback</span>
                                        </div>
                                <div class="modal-body">
                                  <div class="reply-box loginBox feedback">
                                    <div class=" contact">
                                    <div id="fb_msg"></div>
                                      <form class="well" id="feedback_frm" name="feedback_frm"  action="'.base_url('pages/feedback_submit').'" method="post" >
                                        <div class="row">
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="Name ">Enter your Name<sup>*</sup></label>
                                              <input type="text" class="form-control" name="Name" id="fb_name" value=""  required>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="Number">Enter your Phone Number<sup>*</sup></label>
                                              <div class="input-group mobnum"> <span class="input-group-addon"><span>+91</span> </span>
                                                <input type="tel" class="form-control" id="fb_number" name="Mobile" value="" min="0" max="10" maxlength="10"  required/>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group form-radio">
                                              <label for="Email">Enter your E-Mail<sup>*</sup></label>
                                              <input type="email" class="form-control" name="Email" id="fb_email" value="" required >
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone"> 1) How did you hear about us?</label>
                                            </div>
                                            <div class="form-group form-radio radio-group-1" >
                                              <label class="radio-inline" for="fb_social_media" >
                                                <input type="radio" name="HowHear" id="fb_social_media" value="SocialMedia" checked="true">
                                                Social Media</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_advertising" value="Advertising" >
                                                Advertising</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_search_engine" value="SearchEngine" >
                                                Search Engine</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_friend_colleague" value="FriendColleague" >
                                                Friend/colleague</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="HowHear" id="fb_others" value="Others" >
                                                Other</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="OtherTxt" id="fb_other_txt" value="" >
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">
                                              <label for="phone">2) What browser do you use?</label>
                                            </div>
                                            <div class="form-group form-radio radio-group-2">
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_google_crome" value="GoogleCrome" checked="true" >
                                                Google Chrome</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_mozilla_firefox" value="MozillaFirefox" >
                                                Mozilla Firefox</label>

                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_microsoft_edge" value="MicrosoftEdge" >
                                                Microsoft Edge</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_safari" value="Safari" >
                                                Safari</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhatBrowser" id="fb_opera" value="Opera" >
                                                Opera</label>
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio ">
                                            <div class="form-group form-radio">
                                              <label for="phone">3) Which device do you use to access our content?</label>
                                            </div>
                                            <div class="form-group form-radio radio-group-3">
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_large_screen_device" value="LargeScreenDevice" checked="true" >
                                                Large Screen Devices (Desktop/Laptop)</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_medium_screen_device" value="MediumScreenDevice" >
                                                Medium Screen Devices (Tablets/iPads)</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="WhichDevice" id="fb_small_screen_device" value="SmallScreenDevice" >
                                                Smaller Screen Devices(Smartphones)</label>
                                            </div>
                                          </div>
                                          <div class="col-md-12 top-radio">
                                            <div class="form-group form-radio">

                                              <label for="phone">4) Are you satisfied to find us?</label>
                                            </div>
                                            <div class="form-group form-radio radio-group-4">
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_very_satisfied" value="VerySatisfied" checked="true" >
                                                Very Satisfied</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_satisfied" value="Satisfied" >
                                                Satisfied</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_neutral" value="Neutral" >
                                                Neutral</label>
                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_unsatisfied" value="Unsatisfied">
                                                Unsatisfied </label>

                                              <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_very_unsatisfied" value="VeryUnsatisfied" >
                                                Very Unsatisfied</label>

                                                <label class="radio-inline">
                                                <input type="radio" name="AreYouSatisfied" id="fb_s_other" value="SatisfiedOther" >
                                                Other</label>
                                            </div>
                                            <div class="form-group">
                                                <textarea name="Notes" id="fb_notes"  class="form-control" ></textarea>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group btn-send">
                                              <button class="btn btn-send" type="submit" name="submit" value="submit" >Submit Feedback</button>
                                            </div>
                                          </div>

                                        </div>

                                      </form>

                                    </div>

                                  </div>

                                </div>

                                <div class="clearfix"></div>

                              </div>

                            </div>

                          </div>

                        </div>';

    echo $feedback_html;

}

function get_location_popup(){

    $feedback_html = '';

    $feedback_html = '<div class="quickReply">
                            <div class="modal fade" id="gotolocation" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <span class="bg-red">Get to the Location</span>
                                        </div>
                                <div class="modal-body">
                                  <div class="reply-box loginBox feedback">
                                    <div class=" contact">
                                    <div id="fb_msg"></div>
                                      <form class="well" name="feedback_frm"  action="'.base_url('pages/feedback_submit').'" method="post" >
                                        <div class="row">
                                          <div class="col-md-12" id="locationmap">
                                            
                                          </div>
                                        </div>

                                      </form>

                                    </div>

                                  </div>

                                </div>

                                <div class="clearfix"></div>

                              </div>

                            </div>

                          </div>

                        </div>';

    echo $feedback_html;

}



function get_feedback_thank_popup(){

    $feedback_thank_html ='' ;

    $feedback_thank_html ='<div class="quickReply">

                    <div class="modal fade" id="FeedbackThankPop" role="dialog">

                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-body">

                                    <div class="reply-box loginBox">

                                        <p>Thank you for your precious feedback.</p>

                                        <p>We will make sure to make our product better with time.</p>

                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>';

    echo $feedback_thank_html;

}



function get_subscribe_thank_popup(){

    $thank_html ='' ;

    $thank_html ='<div class="quickReply">

                    <div class="modal fade" id="subscribeThankPop" role="dialog">

                        <div class="modal-dialog">

                            <div class="modal-content">

                                <div class="modal-body">

                                    <div class="reply-box loginBox">

                                        <p>Thank you for subscribing us.</p>

                                        <p>We promise we won\'t spam your inbox and will always give you the latest news.</p>

                                        <a href="#" class="businessSubmit" data-dismiss="modal">continue</a>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>';

    echo $thank_html;

}

function get_current_city(){
  $ci =& get_instance();
  $current_city = "";
  $ip = $_SERVER['REMOTE_ADDR'];
  
        $ch = curl_init();
        $url = "http://ipinfo.io/".$ip."/json";
        curl_setopt($ch, CURLOPT_URL,$url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);      
  
  
  
 // $temp_url_result = @file_get_contents("http://ipinfo.io/{$ip}/json");
  $url_result = json_decode( $output );
  $current_city = "";
  if( isset($url_result->city) && $url_result->city ){
     $current_city = $url_result->city;
  }

  return $current_city;
}




function get_footer_section(){

  $ci =& get_instance();
  $current_city = get_current_city();
  $slider_str = "";
  $trending_categories_data = $ci->general_model->get_all_rows(" * ",tbl( "category" )," AND Status='0' AND Parent='0' ORDER BY RAND() LIMIT 30" );
  if( isset( $trending_categories_data ) && count( $trending_categories_data )  ){
    $i = 1;
    $slider_str .= '<div class="col-md-12">
                      <div class="category-box">
                        <div class="owl-carousel owl-theme" id="category-box">';
    foreach( $trending_categories_data as $tc_key => $tc_value) {

        if( $i == 1  ){

            $slider_str .= '<div class="item">';

        }

        $slider_str .= '<div class="cat-box">

                          <h4><a href="javascript:void(0)" class="trendcat1" id="'.$i.'">'.$tc_value['Name'].' in <span class="c_city">'.$current_city.'</span></a></h4>
                           <input type="hidden" id="getcat1'.$i.'" value="'.$tc_value['Name'].'">
                          <p>'.substr($tc_value['Description'], 0, 90).'</p>
                        </div>';

      if( $i%5 == 0  && $i != count( $trending_categories_data )    ){

        $slider_str .= '</div>

                      <div class="item">';

      }

      if( $i == count( $trending_categories_data )    ){

        $slider_str .= '</div>';

      }

      $i++;
    }


    $slider_str .= '</div>
                  </div>
                  <div class="clearfix"></div>
              </div>';

  }

  //<li><a href="'.base_url("pages/news").'">in the news</a></li>
  //<li><a href="#subscribeUs" data-toggle="modal" >subscribe us</a></li>
  //<li><a href="#">social media</a></li>
  $footer_html = '<section class="widget-sec">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-2 col-sm-6 col-xs-6 xs-full">

                                <div class="widget-box">
                                    <h4>about us</h4>
                                    <ul>
                                        <li><a href="'.base_url("pages/about-us").'">company overview</a></li>
                                        <li><a href="'.base_url("pages/policy").'">Policy</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-6 xs-full">

                                <div class="widget-box">

                                    <h4>news & resources</h4>

                                    <ul>
                                        

                                        <li><a href="'.base_url("pages/video").'">videos</a></li>
                                        

                                        <!--<li><a href="#">career at SMDC</a></li>-->

                                    </ul>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-6 xs-full">

                                <div class="widget-box">
                                    <h4>reach us</h4>
                                    <ul>
                                        <li><a href="#" onclick="show_business();" >add business</a></li>
                                        <li><a href="'.base_url("pages/contact-us").'">contact us</a></li>
                                        <li><a href="#" onclick="show_feedback();" >submit feedback</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-6 xs-full">

                                <div class="paymentMmode">

                                    <h4>payment mode</h4>
                                    <ul>
                                        <li><a href="#"><img src="'.base_url("assets/img/visa.png").'" class="img-responsive" alt=""></a></li>
                                        <li><a href="#"><img src="'.base_url("assets/img/mastercard-1.png").'" class="img-responsive" alt=""></a></li>

                                        <li><a href="#"><img src="'.base_url("assets/img/mastercard.png").'" class="img-responsive" alt=""></a></li>

                                        <li><a href="#"><img src="'.base_url("assets/img/american-express.png").'" class="img-responsive" alt=""></a></li>

                                        

                                        <li><a href="#"><img src="'.base_url("assets/img/Net-Banking.png").'" class="img-responsive" alt=""></a></li>

                                    </ul>

                                </div>

                            </div>

                            <div class="clearfix"></div>

                            '.$slider_str.'

                        </div>

                    </div>

                </section>



                <section class="footer-sec">

                    <div class="dividerLine"></div>

                    <div class="container">

                        <div class="row">

                            <div class="col-md-9 col-sm-8 col-xs-12">

                                <div class="footer-box">

                                    <ul>

                                        <!--<li><a href="#">Careers</a></li>-->
                                        <li><a href="'.base_url("pages/policy").'">Privacy Policy</a></li>

                                        <li><a href="#">Copyright © 2017. Telcon Advertisements Pvt. Ltd. All rights reserved.</a>

                                        </li>

                                    </ul>

                                </div>

                            </div>

                            <div class="col-md-3 col-sm-4 col-xs-12 ">
                                <div class="footer-social">
                                    <ul>
                                        <li>
                                            <a href="https://www.instagram.com/searchmaadi/" target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/nammsearchmaadi/status/941275649582362625" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                        <a href="https://www.facebook.com/nammasearchmaadi/" target="_blank">
                                            <i class="fa fa-facebook-f"></i>
                                        </a>

                                        </li>

                                    </ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </section>

                <div class="scrollTop">

                    <a href="#" id="scrollTop">

                      <i class="fa fa-arrow-up" aria-hidden="true"></i>

                    </a>

                </div>';

                /*<li><a href="#">Disclaimer</a></li>
                // payment card
                <li><a href="#"><img src="'.base_url("assets/img/diners-club.png").'" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="'.base_url("assets/img/discover.png").'" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="'.base_url("assets/img/RuPay.png").'" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="'.base_url("assets/img/PhonePe.png").'" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="'.base_url("assets/img/COD.png").'" class="img-responsive" alt=""></a></li>
                <li><a href="#"><img src="'.base_url("assets/img/EMI-Option.png").'" class="img-responsive" alt=""></a></li>
                */

  echo $footer_html;

}



